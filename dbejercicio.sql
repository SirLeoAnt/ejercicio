/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : dbejercicio

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-03-08 20:58:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cat_usuario
-- ----------------------------
DROP TABLE IF EXISTS `cat_usuario`;
CREATE TABLE `cat_usuario` (
  `use_id` int(11) NOT NULL AUTO_INCREMENT,
  `niv_id` int(11) NOT NULL,
  `use_usuario` varchar(45) DEFAULT NULL,
  `use_password` varchar(45) DEFAULT NULL,
  `use_estado` tinyint(4) DEFAULT NULL,
  `usu_alta` int(10) unsigned NOT NULL,
  `usu_fechaAlta` datetime NOT NULL DEFAULT '2017-01-30 00:00:00',
  `usu_cambio` int(10) unsigned NOT NULL,
  `usu_fechaCambio` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`use_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cat_usuario
-- ----------------------------
INSERT INTO `cat_usuario` VALUES ('1', '1', 'leo', 'leo', '1', '0', '2017-01-30 00:00:00', '0', '2018-03-08 14:23:54');
INSERT INTO `cat_usuario` VALUES ('2', '1', 'LEO2', '123', '2', '1', '2018-03-08 14:46:36', '0', '2018-03-08 14:46:36');
