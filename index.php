<?php
	session_start(); 
?>
<!doctype html>
<html lang="es">
	<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/bootstrap.3.3.6.min.css" />
	<!--<link rel="stylesheet" href="css/bootstrap-theme.3.3.6.min.css" />-->
	<link rel="stylesheet" href="css/bootstrap-united-modify.min.css" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Baloo+Da" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<!--<link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet"/>-->
	<link href="css/jquery-ui-1.11.4.min.css" rel="stylesheet"/>
	<link href="css/select2-4.0.0.min.css" rel="stylesheet"/>
	<link href="fonts/octicons/octicons.css" rel="stylesheet"/>
	<link href="css/font-awesome.4.6.3.min.css" rel="stylesheet"/>
	<link href="css/materialdesign.css" rel="stylesheet"/>
	<link href="css/upfile.css" rel="stylesheet"/>
	<link href="css/rim.css" rel="stylesheet"/>
	<link rel="icon" type="image/x-icon" href="css/xicon.ico" />
	<link href="css/ddapp.css?luk=2" rel="stylesheet"/>
	<link href="css/summernote-0.6.6.css" rel="stylesheet"/>
	<link href="css/bootstrap-colorpicker-2.1.min.css" rel="stylesheet"/>
	<link href="css/datepicker3.css" rel="stylesheet"/>
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
	<link rel="stylesheet/less" type="text/css" href="less/relox.less" />
	<script src="js/less-2.5.0.min.js" type="text/javascript"></script>
	<!--<script src="js/jquery-1.9.1.js"></script>-->
	<script src="js/jquery-1.11.2.min.js"></script>
	<!--<script src="js/jquery-ui-1.10.3.custom.js"></script>-->
	<script src="js/jquery-ui-1.11.4.min.js"></script>
	<script src="js/bootstrap-3.3.4.min.js"></script>
	<script src="js/select2.full-4.0.0.min.js"></script>
	<script src="js/i18n/select2_locale_es.js"></script>
	<script src="js/jqajax-3.0.2.js"></script>
	<!--<script src="js/jquery.cookie.js"></script>-->
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>
	<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>

	<!--<script src="js/highcharts-4.1.5.js"></script>
	<script src="js/modules/data.js"></script>
	<script src="js/modules/drilldown.js"></script>-->
	<script src="js/summernote-0.6.6.min.js"></script>
	<script src="js/i18n/summernote-es-ES.js"></script>
	<script src="js/bootstrap-colorpicker.min-2.1.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>	
	<script src="js/i18n/jquery.ui.datepicker-es.js"></script>	
	<script src="js/i18n/bootstrap-datepicker.es.js" charset="UTF-8"></script>
	<script src="js/markupMap.js"></script>
	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
	<style id="cssfileload" >
		.fileinput-button {
		position: relative;
		overflow: hidden;
		}
		.fileupload-buttonbar .btn, .fileupload-buttonbar .toggle {
		margin-bottom: 5px;
		margin-rigth: 3px;
		}
		.fileinput-button input {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		opacity: 0;
		-ms-filter: 'alpha(opacity=0)';
		font-size: 200px;
		direction: ltr;
		cursor: pointer; 
		}
	</style>
	<style>.datepicker{z-index:1200 !important;}</style>
	<!--<script type="text/javascript" src="https://www.google.com/jsapi"></script >-->
	<!--<script src="js/i18n/bootstrap-datepicker.es.js"></script> perdido -->
	</head>
	<body>
		<div id="dda">
			<!--
			<center id="dda-banner"></center>
			<div id="dda-header" class="affix" style="width:100%; z-index:3; top:0">
				
				<div class="col-xs-8 col-xs-offset-2">
					<center><img id="dda-enc" class="img-rounded"></center>
				</div>
				
				max-height: 550px; overflow: scroll
			</div>
			--> 
			<!--<div id="dda-nav" class="col-xs-12 affix affix-top" style="top:0px; z-index: 3"></div>-->
			
			<div id="dda-header" class="affix" style="width:100%; z-index:3; top:0">
				<div class="row">
					<div class="col-xs-2"><center><img src=""></center></div>
					<div class="pull-right" style="margin-right:10px;"><img src=""></div>
				</div>
			</div>
			<div class="affix" class="row" style="z-index:3; top:85px; right:15px;">				
				<div class="row" style="background:#e3e3e3; border:1px solid #e7e7e7; color:#2E0000; padding:2px;">
					<a href="salir.php" class="crs-pnt" style="float:right" >
						<i class="material-icons">&#xE572;</i> SALIR
					</a>
				</div>
			</div>
				
			<div class="container-fluid">
				<div class="row">
					<div id="dda-nav" class="col-xs-2"></div>
					<div class="col-xs-10">
						<div id="dda-content" class="container-fluid" style="min-height:200px;"></div>
					</div>
				</div>
			</div>
			<div id="dda-floor" class="affix affix-bottom">
				<div class="row">
					<div class="pull-right" style="color:#fbfbfb; padding:10px 30px;">
					</div>
				</div>
			</div>
		</div>
		
		<div id="modal_logueo" class="modal fade" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog">
				<div class="modal-content modal-sm" style="width:410px">
					<!--<div class="modal-header"></div>-->
					<div class="modal-body" style="padding-bottom:0px;">
						<div class="form-horizontal">
							<div class="row">
								<center><em style="color:#9683A6;"> EJERCICIO</em></center>
								<center><em style="color:#9683A6;"> version: 0.0.1<b>α</b></em></center>
								
								<div class="col-xs-6 col-xs-offset-3" style="margin-top:15px;">
								
									<div class="form-group">
										<div class="col-xs-12">
											<div class="input-group">
											  <span class="input-group-addon" id="basic-addon1">
												<i class="fa fa-user"></i>
											  </span>
											  <input type="text" id="dda-usu" placeholder="usuario" class="form-control" aria-describedby="basic-addon1"/>
											</div>
											
											
										</div>
									</div>						
									<div class="form-group">
										<div class="col-xs-12">
											<div class="input-group">
											  <span class="input-group-addon" id="basic-addon1">
												<i class="fa fa-qrcode" aria-hidden="true"></i>
											  </span>
											  <input type="password" id="dda-pass" placeholder="contrase&ntilde;a" class="form-control" aria-describedby="basic-addon1"/>
											</div>
										</div>
										
									</div>
									
									<div class="form-group" style="margin-top:30px;">
										<div class="col-xs-12">
											<center><button id="dda-ini" data-dismiss="modal" class="btn btn-sm btn-success"><i class="fa fa-power-off"></i> Entrar</button></center>
										</div>
										<div class="col-xs-12">
											<center><button data-dismiss="modal" id="dda-reg" class="btn btn-sm btn-danger"><i class="fa fa-users"></i> Registrar</button></center>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	
		<div id="mdlAlert" class="modal fade">
			<div class="modal-dialog" style="width: 356px;">
				<div class="modal-content"> 
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<center></center>
					</div>
					<div id="bdyMdlAlert" class="modal-body">
					</div>
				</div>
			</div>
		</div>
	
		
	</body>
	
	<script type="text/javascript">
		$(function() {	
			<?php
			if (!isset($_SESSION['sso_id'])){
				if (isset($_SESSION['sso_id'])=='0'){
					session_destroy();
				}
			}
			if (!isset($_SESSION['sso_id'])){
				echo '$("#modal_logueo").modal("show");';
			} else {
				echo 'comunica("map/cMain.php",{metodo:"main"});';
			}
			?>
			$("#dda-usu").keypress(function(e) {
				if(e.which == 13) { $("#dda-pass").focus();	}
			});
			$("#dda-pass").keypress(function(e) {
				if(e.which == 13) { $("#dda-ini").click();	}
			});
			$("#dda-ini").click( function(){
				comunica("map/cMain.php",{
					metodo:"login",
					usuario:$("#dda-usu").val(),
					pass:$("#dda-pass").val(),
					ancho: screen.width,
					largo: screen.height				
				}, [{c:"dda-content",t:"fb"}] );
			});
			$("#dda-reg").click( function(){
				comunica("map/cUsuario.php",{}, [{c:"dda-content",t:"fb"}] );
			});
			$("#dda-usu").focus();
			$( document ).on( "click","a.filtros-choice-close", function() {
				$(this).parent().remove();
			});
		});
		
	</script>	
	
</html>