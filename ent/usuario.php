<?php
require_once dirname(__FILE__)."/Entidad.php";
class usuario extends Entidad{
/****************************************************** 
		ATRIBUTOS DE LA CLASE
******************************************************/
	protected $use_id;	//
	protected $niv_id;	//
	protected $usuario;	//
	protected $password;	//
	protected $estado;	//
/****************************************************** 
		METODOS GET
******************************************************/
	public function getUse_id() { return $this->use_id->getVal(); }
	public function getNiv_id() { return $this->niv_id->getVal(); }
	public function getUsuario() { return $this->usuario->getVal(); }
	public function getPassword() { return $this->password->getVal(); }
	public function getEstado() { return $this->estado->getVal(); }
/****************************************************** 
		METODOS SET
******************************************************/
	public function setUse_id($val) { $this->use_id->setVal($val); }
	public function setNiv_id($val) { $this->niv_id->setVal($val); }
	public function setUsuario($val) { $this->usuario->setVal($val); }
	public function setPassword($val) { $this->password->setVal($val); }
	public function setEstado($val) { $this->estado->setVal($val); }
/****************************************************** 
		METODOS STA (SET sin persistencia)
******************************************************/
	public function staUse_id($val) { $this->use_id->staVal($val); }
	public function staNiv_id($val) { $this->niv_id->staVal($val); }
	public function staUsuario($val) { $this->usuario->staVal($val); }
	public function staPassword($val) { $this->password->staVal($val); }
	public function staEstado($val) { $this->estado->staVal($val); }
/****************************************************** 
		CONSTRUCTOR
******************************************************/
	public function __construct(){
		if(func_num_args() > 0) $p = ( is_object( func_get_arg(0) ) ) ? func_get_arg(0) : json_decode(func_get_arg(0));
		parent::__construct(array("tabla"=>"cat_usuario","server"=>"map17"));
			/*SE DEFINEN LOS TIPOS DE VARIABLES O ATRIBUTOS DE LA CLASE*/
			$this->use_id = new Atributo("numero","NO","PRI","use_id");
			$this->niv_id = new Atributo("numero","NO","","niv_id");
			$this->usuario = new Atributo("cadena","YES","","use_usuario");
			$this->password = new Atributo("cadena","YES","","use_password");
			$this->estado = new Atributo("numero","YES","","use_estado");
			if (isset($p->use_id)){ 
				$this->staUse_id($p->use_id);
				$this->select();
			}
		}
/****************************************************** 
		FUNCIONES ESTÁTICAS
******************************************************/
	public static function abc($p){
		$use =  new usuario();
		if( isset($p->use_id) ) $use->setUse_id($p->use_id);
		if( isset($p->niv_id) ) $use->setNiv_id($p->niv_id);
		if( isset($p->usuario) ) $use->setUsuario($p->usuario);
		if( isset($p->password) ) $use->setPassword($p->password);
		if( isset($p->estado) ) $use->setEstado($p->estado);
		switch($p->abc){
			case "a": $use->insert(); $use->setUltimoId($use->getUltimoId()); break;
			case "b": $use->delete(); break;
			case "c": $use->update(); break;
		}
		return $use;
	}
}
?>
