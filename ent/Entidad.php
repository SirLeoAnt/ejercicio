<?php
include_once dirname(__FILE__).'/../lib/AccesoDatos.php';
//include_once dirname(__FILE__).'/../lib/Meses.php';
class Atributo {
	private $val;
	private $tipo;
	private $change = false;
	private $null;
	private $keys;
	private $name;	
	public function getVal() { return $this->val; }
	public function getTipo() { return $this->tipo; }
	public function getChange() { return $this->change; }
	public function getNull() { return $this->null; }
	public function getKeys() { return $this->keys; }
	public function getName() { return $this->name; }	
	public function setVal($val) { $this->val=$val; $this->change=true; }
	public function staVal($val) { $this->val=$val; }	
	public function setTipo($val) { $this->tipo=$val; }
	public function setChange($val) { $this->change=$val; }
	public function setNull($val) { $this->null=$val; }
	public function setKeys($val) { $this->keys=$val; }	
	public function setName($name) { $this->name=$val; }		
    public function  __construct($tipo,$null,$keys,$name){ 
		$this->tipo=$tipo;
		$this->null=$null;
		$this->keys=$keys;
		$this->name=$name;
	}
}
class AtributoExterno{
	private $val;
	public function staVal($val) { $this->val=$val; }	
	public function getVal() { return $this->val; }
	public function getTipo() { return $this->tipo; }
	public function getName() { return $this->name; }	
	public function getTabla() { return $this->tabla; }	
	public function  __construct($tipo,$name,$tabla){ 
		$this->tipo=$tipo;
		$this->name=$name;
		$this->tabla=$tabla;
	}
}
class Entidad extends AccesoDatos{
	/******************************************************
	             DECLARACION DE VARIABLES                  
	******************************************************/
	private $me;
	private $table;
	/******************************************************        
	           DECLARACION FUNCIONES GET               
	******************************************************/
	public function getTable() { return $this->table; }
	protected function getMe() { return $this->me; }	
	public function __get($name){ if( isset($this->$name) ) return $this->$name; }
	/******************************************************        
	           DECLARACION FUNCIONES SET               
	******************************************************/
	public function setTable($val) { $this->table = $val; }
	protected function setMe($val) { $this->me = $val; }
	public function __set($name, $val){ if( isset($this->$name) ) $this->$name = $val; }
	/******************************************************        
	           CONSTRUCTOR
	******************************************************/	
	public function __clone() {
        foreach ($this as $key => $val) {
            if (is_object($val) || (is_array($val))) {
                $this->{$key} = unserialize(serialize($val));
            }
        }
    }
	public function __construct( ){
		//Recibiendo los parametros de la Clase Entidad
		$parametros = ( func_num_args() > 0 ) ? ( is_object( func_get_arg(0) ) ) ? func_get_arg(0) : ( is_array( func_get_arg(0) ) ) ? (object)func_get_arg(0) : null : null;
		$this->_myself = $this;
		//parametros de conexion
		parent::__construct( $parametros );
		
		//instanciando un espejo
		$this->setMe( new ReflectionClass($this) );
		
		$tabla = (!isset($parametros->tabla)) ? '' : $parametros->tabla;
		$this->setTable($tabla);
	}
	public function insert(){
		
		$sql = "insert into ".$this->getTable()."( ";		
		$values = " ) values ( ";
		$properties = $this->me->getDefaultProperties();
		
		foreach( $properties as $key => $atributo){
			if( $this->$key instanceof Atributo and $this->$key->getVal()!=''){
				$sql .= $this->$key->getName().", ";
				switch( $this->$key->getTipo() ){
					case 'fecha':
					case 'date':
						$values .= ( $this->$key->getVal() != '' ) ? "'".$this->$key->getVal()."'" : "'0000-00-00 00:00:00'";
						break;
					case 'string':
					case 'cadena':
						// Esta codificacion, utf-8, convierte a mayusculas las vocales acentuadas y �'s						
						$values .= "'".utf8_decode(mb_strtoupper($this->$key->getVal(),'utf8'))."'";
						//$this->$key->getVal()
					break;
					case 'sensitive':
						$values .= "'".utf8_decode($this->$key->getVal())."'";
					break;
					case 'md5': $values .= "'".md5( strtoupper($this->$key->getVal()) )."'";
						break;
					default:
						$values .= $this->$key->getVal();
						break;
				}
				$values .= ", ";
			/* cuando se instancia un objeto de tipo Meses, se hace un tratamiento especial, que recorre los
			elementos, es decir los meses, y los procesa*/
			} else if( $this->$key instanceof Meses ){
				$meses = $this->$key->__get("me")->getDefaultProperties();				
				//se recorren todos los miembros, del atributo
				foreach( $meses as $idMes => $mes ){
					/*se procesan solo los que son atributo*/
					if( $this->$key->__get($idMes) instanceof Atributo and $this->$key->$idMes->getVal()!=''){
						$sql .= $this->$key->$idMes->getName().", ";
						switch( $this->$key->$idMes->getTipo() ){
							case 'cadena':
							case 'string': $values .= mb_strtoupper( "'".$this->$key->$idMes->getVal()."'",'utf8'); break;
							default: $values .= $this->$key->$idMes->getVal(); break;
						}
						$values .= ", ";						
					}	
				}
			}
		}
		//echo $this->getTable();
		if ($this->getTable()!='gen_Mensaje' and $this->getTable()!='xxx' ){
			$sql .= " usu_alta, usu_fechaAlta ";
			$values .=" {$_SESSION['sso_id']}, now() ) ";
		} elseif ($this->getTable()=='xxx') {
			$sql .= " usu_id ";
			$values .=" {$_SESSION['sso_id']} ) ";
		} else {
			$sql = substr($sql,0,-2);
			$values = substr($values,0,-2);
			$values .="  ) ";
		}
		$this->conectar();
		//echo $sql.$values;
		//die();
		$this->setSql($sql.$values);	
		$this->consultar();
		$this->desconectar();
	}
	public function update(){
		$sql = "update ".$this->getTable()." set  usu_cambio=".$_SESSION['sso_id'].", usu_fechaCambio=now(), ";
		$where = " where ";
		
		$properties = $this->me->getDefaultProperties();
		$coma =  false;
		$and = false;
		foreach( $properties as $key => $atributo){
			if( $this->$key instanceof Atributo){ //si es atributo
				if( $this->$key->getKeys() == "PRI"){ //si es llave primaria, se forma el where
					switch( $this->$key->getTipo() ){
						case 'cadena':
						case 'string':	
							$where .= $this->$key->getName()." = '".$this->$key->getVal()."' and ";
							break;
						default:
							$where .= $this->$key->getName()." = ".$this->$key->getVal()." and ";
							break;
					}
					$and =  true;					
				}else if( $this->$key->getChange() ){
					$sql .= $this->$key->getName()." = ";
					switch( $this->$key->getTipo() ){
						case 'fecha':
						case 'date':
							//$sql .= ( $this->$key->getVal() != "" ) ? "'".$this->date_to_mysqldate( $this->$key->getVal() )."'" : "null";
							//break;
						case 'cadena':
						case 'string':
							//Esta codificacion, utf-8, convierte a mayusculas las vocales acentuadas y �'s
							// $sql .= utf8_decode(mb_strtoupper( "'".$this->$key->getVal()."'",'UTF-8'));
							$sql .= "'".utf8_decode( mb_strtoupper($this->$key->getVal(),'utf8'))."'";							
							//$sql .= mb_strtoupper( "'".$this->$key->getVal()."'",'UTF-8');									
							break;
						case 'sensitive':
							$sql .= "'".utf8_decode($this->$key->getVal())."'";
						break;
						case 'md5': 
								$sql .= "'".md5( strtoupper( $this->$key->getVal() ))."'";
							break;
						default:
							if ($this->$key->getVal() != '')
								$sql .= $this->$key->getVal();
							else
								$sql .= '0';
							break;
					}
					$sql .= ", ";
					$coma = true;
				}
			} 
		}
		if($coma) $sql = substr( $sql, 0, -2); //si hay, se quita la ', '
		if($and) $where = substr( $where, 0, -5); //si hay se quita el ' and '
		
		$this->conectar();	
		$this->setSql($sql.$where);	
		//echo	$this->getSql($sql.$where);	
		$this->consultar();		
		$this->desconectar();
	}	
	public function delete(){
		$sql = "delete from ".$this->getTable();		
		$where = " where ";
		
		$properties = $this->me->getDefaultProperties();		
		$and =  false;
		foreach( $properties as $key => $atributo){
			if( $this->$key instanceof Atributo){ //si es atributo
				if( $this->$key->getKeys() == "PRI"){ //si es llave primaria, se forma el where
					switch( $this->$key->getTipo() ){
						case 'fecha':
						case 'cadena':
						case 'string':	
							$where .= $this->$key->getName()." = '".$this->$key->getVal()."' and ";
							break;
						default:
							$where .= $this->$key->getName()." = ".$this->$key->getVal()." and ";
							break;
					}
					$and =  true;
				}
			}
		}
		if($and) $where = substr( $where, 0, -5); //si hay se quita el ' and '
		
		$this->conectar();		
		$this->setSql($sql.$where);
		// echo	$this->getSql();
		$this->consultar();		
		$this->desconectar();
	}
	public function select(){
		$sql = "select ";
		$seljoin = '';
		$from = " from ".$this->getTable();
		$where = " where ";
		$properties = $this->me->getDefaultProperties();
		$coma =  false;
		$and =  false;
		foreach( $properties as $key => $atributo){
			if( $this->$key instanceof Atributo){ //si es atributo
				if( $this->$key->getKeys() == "PRI"){ //si es llave primaria, se forma el where
					switch( $this->$key->getTipo() ){
						case 'cadena':
						case 'string':	
							$where .= $this->$key->getName()." = '".$this->$key->getVal()."' and ";
							break;
						default:
							$where .= $this->$key->getName()." = ".$this->$key->getVal()." and ";
							break;
					}
					$and =  true;
				} else {
					//switch( $this->$key->getTipo() ){
						//case 'blob':
							//$sql .= "CONVERT( ".$this->$key->getName()." USING utf8) ".$this->$key->getName().", ";
						//default:
							$sql .= $this->$key->getName().", ";
					//}
					$coma = true;
				}
			/*cargando elementos de tipo Meses*/
			} else if ( $this->$key instanceof AtributoExterno ) {
				//print_r($this->$key);
				//$this->prm->a_join
				if (in_array($this->$key->getTabla(),$this->a_join))
					$sql .= $this->$key->getName().", ";
			} else if( $this->$key instanceof Meses){
				$meses = $this->$key->__get("me")->getDefaultProperties();				
				foreach( $meses as $idMes => $mes ){					
					if( $this->$key->__get($idMes) instanceof Atributo ){
						$sql .= $this->$key->$idMes->getName().", ";
						$coma = true;
					}	
				}
			}
		}
		
		if($coma) $sql = substr( $sql, 0, -2); //si hay, se quita la ', '
		if($and) $where = substr( $where, 0, -5); //si hay se quita el ' and '
		
		
		
		$this->conectar();		
		
		$this->setSql($sql.$from.$this->join.$where);
		// echo	$this->getSql($sql.$from.$where);
		$this->consultar();
		$row = $this->extraer();		
		$this->desconectar();
		
		foreach( $properties as $key => $atributo ){
			if( $this->$key instanceof Atributo){
				if( $this->$key->getKeys() != "PRI"){ //si no es llave primaria
					switch( $this->$key->getTipo() ){
						case 'date':
						/*case 'fecha':
							$this->$key->staVal( $this->mysqldate_to_date( $row[ $this->$key->getName() ] ) );
							break;
							*/
						default:
							$this->$key->staVal( $row[ $this->$key->getName() ] );
						break;
					}					
				}
			} else if ( $this->$key instanceof AtributoExterno ) {
				if (in_array($this->$key->getTabla(),$this->a_join)){
				switch( $this->$key->getTipo() ){
						/*case 'date':
							$this->$key->staVal( $this->mysqldate_to_date( $row[ $this->$key->getName() ] ) );
							break;
						*/
						default:
							$this->$key->staVal( $row[ $this->$key->getName() ]);
						break;
					}	
				}
			} else if ( $this->$key instanceof Meses) {
				// $meses = $this->$key->__get("me")->getDefaultProperties();				
				foreach( $meses as $idMes => $mes ){					
					if( $this->$key->__get($idMes) instanceof Atributo ){
						switch( $this->$key->$idMes->getTipo() ){
							default: 
								$this->$key->$idMes->staVal( $row[ $this->$key->$idMes->getName() ] );
							break;
						}	
						
					}	
				}
			}
		}		
		//var_dump($this);
	}
	public function copy(){
		
		$sql = "insert into ".$this->getTable()."( ";		
		$values = " ) values ( ";
		$properties = $this->me->getDefaultProperties();
		
		foreach( $properties as $key => $atributo){
			if( $this->$key instanceof Atributo and $this->$key->getVal()!=''){
				$sql .= $this->$key->getName().", ";
				switch( $this->$key->getTipo() ){
					case 'fecha':
					case 'date':
						$values .= ( $this->$key->getVal() != '' ) ? "'".$this->$key->getVal()."'" : "'0000-00-00 00:00:00'";
						break;
					case 'string':
					case 'cadena':
						// Esta codificacion, utf-8, convierte a mayusculas las vocales acentuadas y �'s						
						$values .= "'".$this->$key->getVal()."'";
					break;
					case 'sensitive':
						$values .= "'".utf8_decode($this->$key->getVal())."'";
					break;
					case 'md5': $values .= "'".md5( strtoupper($this->$key->getVal()) )."'";
						break;
					default:
						$values .= $this->$key->getVal();
						break;
				}
				$values .= ", ";
			/* cuando se instancia un objeto de tipo Meses, se hace un tratamiento especial, que recorre los
			elementos, es decir los meses, y los procesa*/
			} 
		}
		//echo $this->getTable();
		if ($this->getTable()!='gen_Mensaje' and $this->getTable()!='xxx' ){
			$sql .= " usu_alta, usu_fechaAlta ";
			$values .=" {$_SESSION['sso_id']}, now() ) ";
		} elseif ($this->getTable()=='xxx') {
			$sql .= " usu_id ";
			$values .=" {$_SESSION['sso_id']} ) ";
		} else {
			$sql = substr($sql,0,-2);
			$values = substr($values,0,-2);
			$values .="  ) ";
		}
		$this->conectar();
		//echo $sql.$values;
		//die();
		$this->setSql($sql.$values);	
		$this->consultar();
		$this->desconectar();
	}
	public function historial(){
		$sql = "insert into log_".$this->getTable()."( ";		
		$values = " ) values ( ";
		$properties = $this->me->getDefaultProperties();
		foreach( $properties as $key => $atributo){
			if( $this->$key instanceof Atributo and $this->$key->getVal()!=''){
				$sql .= $this->$key->getName().", chg_".$this->$key->getName().", ";
				$chg = ( $this->$key->getChange() )?1:0;
				switch( $this->$key->getTipo() ){
					case 'fecha':
					case 'date':
						$values .= ( $this->$key->getVal() != '' ) ? "'".$this->date_to_mysqldate( $this->$key->getVal() )."'" : "'0000-00-00 00:00:00', ".$chg;
						break;
					case 'string':
					case 'cadena':
						// Esta codificacion, utf-8, convierte a mayusculas las vocales acentuadas y �'s						
						$values .= mb_strtoupper( "'".$this->$key->getVal()."'",'UTF-8').", ".$chg;
						break;
					case 'sensitive':
						$values .= "'".$this->$key->getVal()."', ".$chg;
					break;
					case 'md5': $values .= "'".md5( strtoupper($this->$key->getVal()) )."', ".$chg;
						break;
					default:
						$values .= $this->$key->getVal().", ".$chg;
						break;
				}
				$values .= ", ";
			/* cuando se instancia un objeto de tipo Meses, se hace un tratamiento especial, que recorre los
			elementos, es decir los meses, y los procesa*/
			} else if( $this->$key instanceof Meses ){
				$meses = $this->$key->__get("me")->getDefaultProperties();				
				//se recorren todos los miembros, del atributo
				foreach( $meses as $idMes => $mes ){
					/*se procesan solo los que son atributo*/
					if( $this->$key->__get($idMes) instanceof Atributo and $this->$key->$idMes->getVal()!=''){
						$sql .= $this->$key->$idMes->getName().", chg_".$this->$key->$idMes->getName().", ";
						$chg = ( $this->$key->$idMes->getChange() )?1:0;
						switch( $this->$key->$idMes->getTipo() ){
							case 'cadena':
							case 'string': $values .= mb_strtoupper( "'".$this->$key->$idMes->getVal()."'",'UTF-8').", ".$chg; break;
							default: $values .= $this->$key->$idMes->getVal().", ".$chg; break;
						}
						$values .= ", ";						
					}	
				}
			}
		}
		$sql = substr($sql,0,-2);
		$values = substr($values,0,-2);
		$values .="  ) ";
		$this->conectar();
		//echo $sql.$values;
		//die();
		$this->setSql($sql.$values);	
		$this->consultar();
		$this->desconectar();
	}
	public function carga($t){
		$this->conectar();
		/* padre */
		$properties = $t->me->getDefaultProperties();
		$using = '';
		$where = '';
		foreach( $properties as $key => $atributo){
			if( $t->$key instanceof Atributo){ //si es atributo
				if( $t->$key->getKeys() == 'PRI'){ //si es llave primaria, se forma el using
					$using .= $t->$key->getName().',';
					switch( $t->$key->getTipo() ){
						case 'cadena':
						case 'string':	
							$where .= $t->$key->getName()." = '".$t->$key->getVal()."' and ";
							break;
						default:
							$where .= $t->$key->getName()." = ".$t->$key->getVal()." and ";
							break;
					}
				} /*elseif ( $t->$key instanceof AtributoExterno ) {
					if (in_array($t->$key->getTabla(),$t->a_join))
						$sql .= $t->$key->getName().", ";
				}*/
			}
		}
		$using = substr( $using, 0, -1); //le quita la ', '
		$where = substr( $where, 0, -5); //le quita el ' and '
		/****/
		$sql = 'SELECT * FROM '.$this->getTable().' JOIN '.$t->getTable().' USING ('.$using.') '.$this->join.' WHERE '.$where;
		//print_r($t);
		if (isset($t->order))
			if ($t->order!='')
				$sql.=' ORDER BY '.$t->order;
		$this->setSql($sql);
		//echo $sql;
		$this->consultar();
		//echo $this->getSql();
	}
	public function descarga($row){
		$properties = $this->me->getDefaultProperties();
		foreach( $properties as $key => $atributo ){
			if( $this->$key instanceof Atributo){
				//if( $this->$key->getKeys() != "PRI"){ //si no es llave primaria
					switch( $this->$key->getTipo() ){
						case 'date':
							$this->$key->staVal( $this->mysqldate_to_date( $row[ $this->$key->getName() ] ) );
							break;
						default:
							$this->$key->staVal( utf8_decode($row[ $this->$key->getName() ]) );
						break;
					}					
				//}
			} else if ( $this->$key instanceof AtributoExterno ) {
				if (in_array($this->$key->getTabla(),$this->a_join)){
				switch( $this->$key->getTipo() ){
						case 'date':
							$this->$key->staVal( $this->mysqldate_to_date( $row[ $this->$key->getName() ] ) );
							break;
						default:
							$this->$key->staVal( utf8_decode($row[ $this->$key->getName() ]) );
						break;
					}	
				}
			} else if ( $this->$key instanceof Meses) {
				// $meses = $this->$key->__get("me")->getDefaultProperties();				
				foreach( $meses as $idMes => $mes ){					
					if( $this->$key->__get($idMes) instanceof Atributo ){
						switch( $this->$key->$idMes->getTipo() ){
							default: 
								$this->$key->$idMes->staVal( $row[ $this->$key->$idMes->getName() ] );
							break;
						}	
						
					}	
				}
			}
		}
		return clone $this;
	}
	/******************************************************        
	           PERSISTENCIA DE DATOS
	******************************************************/
	public function mysqldate_to_date($fecha){ 		
		
		date_default_timezone_set('America/Mexico_City');
		
		if($fecha == '' or $fecha == '0000-00-00')
			return "";
		else 
			return date("d/m/Y", strtotime($fecha) ); 	
	}
	
	public function date_to_mysqldate($fecha){
		if( !strcmp($fecha, "now()") )
			return $fecha;
		else if($fecha != ""){
			$trozos = explode("/",$fecha,3);
			if( strlen($trozos[0])== 2 ){
				return $trozos[2]."/".$trozos[1]."/".$trozos[0];
			}
			else{ /*si ya viene en formato a�o, mes, dia*/
				return $trozos[0]."/".$trozos[1]."/".$trozos[2];
			}
		}
		return "NULL"; 
	}
	
}
?>