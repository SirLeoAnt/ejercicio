<?php 
session_start();
require_once dirname(__FILE__).'/../lib/wr-3.0.0.php';
require_once dirname(__FILE__)."/../ent/usuario.php";
$parametros = ( isset($_POST['parametros']) ) ? json_decode(json_encode($_POST['parametros'])) : new stdClass();
class CMain extends WebResponse{
	private $prm;	
	private $pantalla='map/cMain.php';
	public function __construct($parametros){
		parent::__construct();		
		$this->prm = $parametros;
		$metodo= (isset($this->prm->metodo) ) ? $this->prm->metodo : '_constructor';
		switch($metodo){
			case '_constructor':case 'main': $this->main(); break;
			case 'login':$this->login(); break;
			case 'sqlPantalla':$this->sqlPantalla(); break;
			default: $this->mixScript('alert("opcion no valida");'); break;
		}
		$this->returnJson();
	}
	/* 
	 * Debe comparar el captcha capturado con el generado, 
	 * si pasa la prueba debe comparar al usuario con su clave
	 * @param string $this->prm->usuario
	 * @param string $this->prm->clave
	 */
	private function login(){
		$s = '';
		$fail = false;
		$_SESSION['sso_id'] = '1';
		
		$this->conectar();
		$this->setSql('
		SELECT  
			use_id, niv_id, use_usuario, use_password, use_estado
		FROM cat_Usuario 
		WHERE use_usuario="'.strtoupper($this->prm->usuario).'" ');
		
		// echo $this->getSql();
		$this->consultar();
		if ($this->getNumRows()>0){
			$row = $this->extraer();
			
				if (  $row['use_password'] == $this->prm->pass ) {
				
				$_SESSION['sso_id']=$row['use_id'];
				$_SESSION['sso_usu']=$row['use_usuario'];
				$_SESSION['sso_niv']=$row['niv_id'];
				switch ( $_SESSION['sso_niv'] ){
					case '4': case '5': 
						$this->setSql('SELECT ua_nombre FROM cat_Aplicativas WHERE ua_id='.$row['ua_id']);
						$this->consultar();
						$row = $this->extraer();
						$_SESSION['sso_nur']=$row['ua_nombre'];
					break;
					case '1': 
						$_SESSION['sso_nur']='MASTER-';
					break;
					case '2':
					break;
					default:
						echo 'nivel no valido';
						die();
					break;
				}
				// $this->navBar();
				$this->mixScript('location.reload();');
			} else {
				$s .= 'alert("Contraseña incorrecta");
				$("#modal_logueo").modal("show");';
			}
			
		} else {
			$s .= 'alert("No es la clave");
			$("#modal_logueo").modal("show");';
		}
		// $s.='$("#mdlLogin").modal("hide");';	
		// 
		//( $_SESSION['sso_id'] )
		$this->mixScript($s);
	}
	/* 
	 * Muestra la barra de menú según los permisos del usuario
	 * 
	 */
	private function navBar(){
		$h='';
		$h='<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" class="affix" style="z-index:3; top:0">';
			$h.='<div class="panel panel-default">';
				$h.='<div id="collapse" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading">';
					$h.='<ul class="list-group">';
						$h.='<li class="list-group-item opcion crs-pnt" href="cUsuario"><i class="fa fa-user"></i> Usuarios</li>';
					$h.='</ul>';
				$h.='</div>';
			$h.='</div>';
		$h.='</div>';
		
		$this->mixScript('
			$("#accordion").collapse();
			$(".desh").click( function(){
				alert("el usuario ha sido deshabilitado hasta nuevo aviso");
				
			});
		');
		$h.='</div>';
		$s='
		$(".git").click(function(){
			alert("Modulo del sistema en desarrollo... ");
		});
		$("#nav-sg .navbar-nav .tipnav").tooltip();
 		$(".opcion").click(function(){
			comunica("map/"+$(this).attr("href")+".php",{},[{c:"dda-content",t:"fb"}]);
			$(".dropdown").removeClass("open");
			return false; 
		});';
		
		
		$this->mixHtmlContenedor( '','dda-content');
			
		$this->mixScript($s);
		$this->mixHtmlContenedor($h,'dda-nav');
		
		$h='';
		$h.='<div class="col-xs-2">';
			$h.='<center><img src="" /></center>';
		$h.='</div>';
		$h.='<div class="pull-right" style="margin-right:10px;">';
			$h.='<img src="" />';
		$h.='</div>';
		$this->mixHtmlContenedor($h,'dda-header');
	}
	private function main(){
		$this->navBar();
	}
	private function frmCom01(){
		$h='';
		$h.='<div class="col-xs-5" style="margin-top:30px;">';
			$h.='<div class="alert alert-info" role="alert">';
				$h.='<strong>Circular No. 8c/0091/2017 </strong>';
				$h.='<br>';
				$h.='<em>9 de enero de 2017</em>';
				$h.='<hr>';
				$h.='<p>En relaci&oacute;n al cierre 2016 del presupuesto asignado a trav&eacute;s del Fondo de Aportaciones para los Servicios de Salud ( FASSA ).</p>';
				$h.='<a href="doctos/OF0091.PDF" target="_blank" class="btn btn-link">';
					$h.='<img src="img/descarga_PDF.png" title="Descargar Circular No. 8c/0091/2017" width="50">';
					$h.='descargar circular';
				$h.='</a>';
			$h.='</div>';
		$h.='</div>';
		
		$h.='<div class="col-xs-5" style="margin-top:30px;">';
			$h.='<div class="alert alert-warning" role="alert">';
				$h.='<strong>Circular No. 8c/0002/2017 </strong>';
				$h.='<br>';
				$h.='<em>23 de enero de 2017</em>';
				$h.='<hr>';
				$h.='<p>Presentaci&oacute;n de la documentaci&oacute;n FASSA 2016.</p>';
				$h.='<a href="doctos/OF0002.PDF" target="_blank" class="btn btn-link">';
					$h.='<img src="img/descarga_PDF.png" title="Descargar Circular No. 8c/0002/2017" width="50">';
					$h.='descargar circular';
				$h.='</a>';
			$h.='</div>';
		$h.='</div>';
		
		
		$h.='<div class="col-xs-11" style="margin:0 30px;">';
			$h.='<div class="alert alert-warning" role="alert">';
				
				$h.='<object data="doctos/OF0002.PDF" type="application/pdf" style="width:100%; height:600px;" >
					alt : <a href="doctos/OF0002.PDF">8c/0002/2017.PDF</a>
					</object>';
			$h.='</div>';
		$h.='</div>';
		// $h.='<div class="col-xs-11" style="margin:0 30px;">';
			// $h.='<div class="alert alert-warning" role="alert">';
				
				// $h.='<object data="doctos/OF0091.PDF" type="application/pdf" style="width:100%; height:600px;" >
					// alt : <a href="doctos/OF0091.PDF">8c/0091/2017.PDF</a>
					// </object>';
			// $h.='</div>';
		// $h.='</div>';
		
		return $h;
	}
	
	/* devuelve los datos de Usuario
	 * @return $u->usuario | nombre del usuario
	 */
	private function frmComunicado(){
		$h='';
		$this->conectar();
		$this->setSql('SELECT ue_id, com_ueNombre, com_ueTitular, com_ueCargo, com_totalA, com_totalC, com_totalD, com_total, com_numOficio FROM ofi_Comunicado');
		$this->consultar();
		$a = array();
		while( $r = $this->extraer()){
			$o = new stdClass();
			$o->ue_id = $r['ue_id'];
			$o->ueNombre = $r['com_ueNombre'];
			$o->totalA = $r['com_totalA'];
			$o->totalC = $r['com_totalC'];
			$o->totalD = $r['com_totalD'];
			$o->total = $r['com_total'];
			$o->numOfi = $r['com_numOficio'];
			array_push($a,$o);
		}
		$h.='<h1 class="zzo">COMUNICADOS DE PRESUPUESTO</h1>';
		$h.='<hr/>';
		$h.='<table class="table table-bordered table-striped">';
			$h.='<thead><tr>';
				$h.='<th>UE</th>';
				$h.='<th>UNIDAD EJECUTORA</th>';
				$h.='<th>TOTAL A</th>';
				$h.='<th>TOTAL C</th>';
				$h.='<th>TOTAL D</th>';
				$h.='<th>TOTAL</th>';
				$h.='<th>DESCARGA</th>';
			$h.='</tr></thead>';
			$h.='<tbody>';
			foreach($a as $o){
				$h.='<tr>';
					$h.='<td>'.$o->ue_id.'</td>';
					$h.='<td>'.$o->ueNombre.'</td>';
					$h.='<td class="text-right">'.$this->mxMoneda( $o->totalA ).'</td>';
					$h.='<td class="text-right">'.$this->mxMoneda( $o->totalC ).'</td>';
					$h.='<td class="text-right">'.$this->mxMoneda( $o->totalD ).'</td>';
					$h.='<td class="text-right">'.$this->mxMoneda( $o->total ).'</td>';
					$h.='<td class="text-center"><a target="_blank" class="btn btn-default btn-xs" href="doctos/comunicaPresu/comunicaPresu_'.$o->numOfi.'.pdf"><i class="fa fa-file-pdf-o text-danger"></i></a></td>';
				$h.='</tr>';
			}
			$h.='</tbody>';
		$h.='</table>';
		return $h;
	}
	
}
new CMain($parametros);
?>