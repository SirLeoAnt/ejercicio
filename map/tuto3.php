<?php
require('/../lib/pdf/fpdf.php');
require_once dirname(__FILE__).'/../lib/AccesoDatos.php';
class PDF extends FPDF
{
	private $cx;
	
	function Header()
	{
		global $title;

		// Arial bold 15
		$this->SetFont('Arial','B',15);
		// Calculamos ancho y posici�n del t�tulo.
		$w = $this->GetStringWidth($title)+6;
		$this->SetX((210-$w)/2);
		// Colores de los bordes, fondo y texto
		$this->SetDrawColor(0,80,180);
		$this->SetFillColor(230,230,0);
		$this->SetTextColor(220,50,50);
		// Ancho del borde (1 mm)
		$this->SetLineWidth(1);
		// T�tulo
		$this->Cell($w,9,$title,1,1,'C',true);
		// Salto de l�nea
		$this->Ln(10);
	}

	function Footer()	{
		// Posici�n a 1,5 cm del final
		$this->SetY(-15);
		// Arial it�lica 8
		$this->SetFont('Arial','I',8);
		// Color del texto en gris
		$this->SetTextColor(128);
		// N�mero de p�gina
		$this->Cell(0,10,'P�gina '.$this->PageNo(),0,0,'C');
	}

	function ChapterTitle($num, $label)
	{
		// Arial 12
		$this->SetFont('Arial','',12);
		// Color de fondo
		$this->SetFillColor(200,220,255);
		// T�tulo
		$this->Cell(0,6,"Cap�tulo $num : $label",0,1,'L',true);
		// Salto de l�nea
		$this->Ln(4);
	}

	function ChapterBody($file)
	{
		// Leemos el fichero
		$txt = file_get_contents($file);
		// Times 12
		$this->SetFont('Times','',12);
		// Imprimimos el texto justificado
		$this->MultiCell(0,5,$txt);
		// Salto de l�nea
		$this->Ln();
		// Cita en it�lica
		$this->SetFont('','I');
		$this->Cell(0,5,'(fin del extracto)');
	}
	// Cargar los datos
	function LoadData($use)
	{
		// Leer las l�neas del fichero
		$this->cx = new AccesoDatos();
		$this->cx->conectar('map17');
		$this->cx->setSql('
			SELECT use_id,niv_id, use_usuario, use_password, use_estado FROM cat_Usuario WHERE use_id = '.$use.'
		');
		$this->cx->consultar();
		$r = $this->cx->extraer();
		$data = array();
		foreach($r as $k)
			$data[] = explode(';',trim($k));
		return $data;
	}

	// Tabla simple
	function BasicTable($header, $data)
	{
		// Cabecera
		foreach($header as $col)
			$this->Cell(40,7,$col,1);
		$this->Ln();
		// Datos
		foreach($data as $row)
		{
			foreach($row as $col){
				$this->Cell(40,6,$col,1);
			}
		}
	}

	// Una tabla m�s completa
	function ImprovedTable($header, $data)
	{
		// Anchuras de las columnas
		$w = array(40, 35, 45, 40);
		// Cabeceras
		for($i=0;$i<count($header);$i++)
			$this->Cell($w[$i],7,$header[$i],1,0,'C');
		$this->Ln();
		// Datos
		foreach($data as $row)
		{
			$this->Cell($w[0],6,$row[0],'LR');
			$this->Cell($w[1],6,$row[1],'LR');
			$this->Cell($w[2],6,number_format($row[2]),'LR',0,'R');
			$this->Cell($w[3],6,number_format($row[3]),'LR',0,'R');
			$this->Ln();
		}
		// L�nea de cierre
		$this->Cell(array_sum($w),0,'','T');
	}

	// Tabla coloreada
	function FancyTable($header, $data)
	{
		// Colores, ancho de l�nea y fuente en negrita
		$this->SetFillColor(255,0,0);
		$this->SetTextColor(255);
		$this->SetDrawColor(128,0,0);
		$this->SetLineWidth(.3);
		$this->SetFont('','B');
		// Cabecera
		$w = array(40, 35, 45, 40);
		for($i=0;$i<count($header);$i++)
			$this->Cell($w[$i],7,$header[$i],1,0,'C',true);
		$this->Ln();
		// Restauraci�n de colores y fuentes
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(0);
		$this->SetFont('');
		// Datos
		$fill = false;
		foreach($data as $row)
		{
			$this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
			$this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
			$this->Cell($w[2],6,number_format($row[2]),'LR',0,'R',$fill);
			$this->Cell($w[3],6,number_format($row[3]),'LR',0,'R',$fill);
			$this->Ln();
			$fill = !$fill;
		}
		// L�nea de cierre
		$this->Cell(array_sum($w),0,'','T');
}


}

$pdf = new PDF();
$title = 'Lista de Usuarios';
$pdf->SetTitle($title);
$pdf->SetAuthor('');
// T�tulos de las columnas
$header = array('Id', 'Nivel', 'USUARIO', 'PASSWORD', 'ESTADO');
// Carga de datos
$data = $pdf->LoadData($_GET['use_id']);
$pdf->SetFont('Arial','',14);
$pdf->AddPage();
$pdf->BasicTable($header,$data);
$pdf->Output();
?>
