<?php
session_start();
$_SESSION['sso_id'] = '1';
require_once dirname(__FILE__).'/../lib/wr-3.0.0.php';
require_once dirname(__FILE__)."/../ent/Usuario.php";
$parametros = ( isset($_POST['parametros']) ) ? json_decode(json_encode($_POST['parametros'])) : new stdClass();
class cUsuario extends WebResponse{
	private $prm;
	private $pantalla='map/cUsuario.php';
	
	public function __construct($parametros){
		parent::__construct();
		$this->prm = $parametros;
		$metodo= (isset($this->prm->metodo) ) ? $this->prm->metodo : '_constructor';
		switch($metodo){
			case '_constructor': case 'main': $this->main(); break;
			case 'frmUsuario': $this->frmUsuario(); break;
			case 'mdlUsuario': $this->mdlUsuario(); break;
			case 'abcUsuario': $this->abcUsuario(); break;
			case 'lisUsuario': $this->mixHtmlContenedor($this->lisUsuario(),'lisUsuario'); break;
			case 'nextPag': $this->nextPag(); break;
			default: $this->mixScript('alert("metodo no definido");'); break;
		}
		$this->returnJson();
	}
	/* Objeto que contiene la lista de Usuarios registrados 
	 * @author: @Robotonio Huazo
	 * @date: 08 Mar 2018
	 */ 
	private function sqlUsuario(){
		$pag = (!isset($this->prm->pag))?0:$this->prm->pag;
		$this->conectar("map17");
		$this->setSql('SELECT * FROM cat_usuario 
			LIMIT '.($pag * 10).',10 ');
		$a = array();
		$this->consultar();
		while($r = $this->extraer()){
			$o = new stdClass();
			$o->use_id=$r['use_id'];
			$o->usuario=$r['use_usuario'];
			$o->password=$r['use_password'];
			$o->estado=$r['use_estado'];
			array_push($a,$o);
		}
		return $a;
	}
	/* Ventana modal para Usuario
	 * @author: @Robotonio Huazo
	 * @date: 08 Mar 2018
	 */ 
	private function mdlUsuario(){
		$h='';
		$h.='<div id="mdlUsuario" class="modal fade">';
			$h.='<div class="modal-dialog">';
				$h.='<div class="modal-content">';
					$h.='<div class="modal-header">';
						$h.='<button type="button" class="close" data-dismiss="modal" >&times;</button>';
						$h.='<h4 class="modal-title">Usuarios</h4>';
					$h.='</div>';
					$h.='<div id="bdyMdlUsuario" class="modal-body"></div>';
					$h.='<div class="modal-footer">';
					$h.='</div>';
				$h.='</div>';
			$h.='</div>';
		$h.='</div>';
		return $h;
	}
	/* Formulario de Usuario
	 * @author: @Robotonio Huazo
	 * @date: 08 Mar 2018
	 */ 
	private function frmUsuario(){
		$h='';
		$h.='<div class="form-horizontal">';
		if (isset($this->prm->use_id)){
			$use = new Usuario($this->prm);
		}else{
			$use = new Usuario();
		}
		// $use = new Usuario((object)array('use_id:"1"'));
		if( $use->getUse_id() != ''){
			$h.='<input id="use_id" type="hidden" value="'.$use->getUse_id().'" />';
			$h.='<input id="abcUse" type="hidden" value="c" />';
		}else{
			$h.='<input id="abcUse" type="text" value="a" />';
		}
			$h.='<div class="form-group">';
				$h.='<label class="col-xs-3 control-label">Usuario:</label>';
				$h.='<div class="col-xs-7">';
				$h.='<!--//cadena-->';
					$h.='<input id="use_usuario" name="use_usuario" type="text" class="form-control" value="'.$use->getUsuario().'">';
				$h.='</div>';
			$h.='</div>';
			$h.='<div class="form-group">';
				$h.='<label class="col-xs-3 control-label">Contrase&ntilde;a:</label>';
				$h.='<div class="col-xs-7">';
				$h.='<!--//numero-->';
					$h.='<input id="password" name="jur_id" type="text" class="form-control" value="'.$use->getPassword().'">';
				$h.='</div>';
			$h.='</div>';
			$h.='<div class="form-group">';
				$h.='<label class="col-xs-3 control-label">Estado:</label>';
				$h.='<div class="col-xs-7">';
				$h.='<!--//numero-->';
					$h.='<input id="estado" name="estado" type="number" class="form-control" value="'.$use->getEstado().'">';
				$h.='</div>';
			$h.='</div>';
			$h.='<div class="form-group">';
				$h.='<div class="col-xs-7">';
					$h.='<button id="sav-use" class="btn btn-xs btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Guardar Usuario</button>';
				$h.='</div>';
			$h.='</div>';
		$h.='</div>';
		$this->mixScript('
			$("#sav-use").click( function(){
				comunica("'.$this->pantalla.'",{
					metodo:"abcUsuario",
					use_id:$("#use_id").val(),
					usuario:$("#use_usuario").val(),
					password:$("#password").val(),
					estado:$("#estado").val(),
					abc:$("#abcUse").val()
				});
			});
		');
		$this->mixHtmlContenedor($h,'bdyMdlUsuario');
	}
	/* Ventana ABC para Usuario
	 * @author: @Robotonio Huazo
	 * @date: 08 Mar 2018
	 */ 
	private function abcUsuario(){
		$this->prm->niv_id = 1;
		switch($this->prm->abc){
			
			case 'a':
				Usuario::abc($this->prm);
				$this->mixHtmlContenedor('
					<div class="alert alert-success">
						<strong>Guardado!</strong> El usuario fue Guardado.
					</div>',
				'bdyMdlAlert');
				$this->mixScript('$("#mdlAlert").modal()');
			break;
			case 'b':
			break;
			case 'c':
				Usuario::abc($this->prm);
				$this->mixHtmlContenedor('
					<div class="alert alert-success">
						<strong>Actualizado!</strong> El usuario fue Guardado.
					</div>',
				'bdyMdlAlert');
				$this->mixScript('$("#mdlAlert").modal()');
			break;
		}
	}
	/* Ventana NAV para Usuario
	 * @author: @Robotonio Huazo
	 * @date: 08 Mar 2018
	 */ 
	private function navPag(){
		$tot = $this->sqlCuentaTotal(); // total de registros
		$renglones_pagina = 10;
		$maximo_paginas = 10;
		if ( isset($this->prm->pag) ){
			$pagina_inicial = $this->prm->pag;
		} else {
		$pagina_inicial = 0;
		}
		$pagina_ultima = $maximo_paginas + $pagina_inicial;
		$ultimisima_pagina = ceil($tot / $renglones_pagina); // 4
		if( $pagina_ultima > $ultimisima_pagina ) $pagina_ultima = $ultimisima_pagina;
		$h='';
		$h.='<div class="col-xs-10 col-xs-offset-1">';
		$h.='<center>';
		$h.='<caption><nav id="nav_pag" tot="'.$tot.'" pag="'.$pagina_inicial.'">';
		$h.='<ul class="pagination">';
		$bloque_anterior = $pagina_inicial - $maximo_paginas;
		if ($pagina_inicial != 0){
		$h.='<li>';
		$h.='<a href="#" aria-label="Last" class="mov_pag" pag="'.$bloque_anterior.'">';
		$h.='<span aria-hidden="true"><i class="fa fa-backward"></i></span>';
		$h.='</a>';
		$h.='</li>';
		}
		for( $i = $pagina_inicial; $i<$pagina_ultima; $i++ ){
			$active = ($i == $pagina_inicial) ? ' class="active" ' : '';
			$h.='<li'.$active.'><a href="#" class="pag" pag="'.$i.'">'.($i+1).'</a></li>';
		}
		if ($ultimisima_pagina > $pagina_ultima ){
		$h.='<li>';
		$h.='<a href="#" aria-label="Next" class="mov_pag" pag="'.($i).'">';
		$h.='<span aria-hidden="true"><i class="fa fa-forward"></i></span>';
		$h.='</a>';
		$h.='</li>';
		}
		$h.='</ul>';
		$h.='</nav></caption>';
		$h.='</center>';
		$h.='</div>';
		$this->mixScript('
			$(".pag").click( function(){
				$("#nav_pag > .pagination li").removeClass("active");
				$(this).parent().addClass("active");
				$("#nav_pag").attr("pag", $(this).attr("pag") );
				comunica("'.$this->pantalla.'",{
					metodo:"lisUsuario",
					pag:$(this).attr("pag")
				},[{c:"lisUsuarios",t:"pez"}]);
			});
		$(".mov_pag").click( function(){
		comunica("'.$this->pantalla.'",{
			metodo:"nextPag",
			pag:$(this).attr("pag")
		},[{c:"pag_natn",t:"circle"}]);
		});
		');
		return $h;
	}
	/* Ventana ContarTotal para Usuario
	 * @author: @Robotonio Huazo
	 * @update: @SirLeoAnt
	 * @date: 08 Mar 2018
	 */ 
	private function sqlCuentaTotal(){
		// $w_mes =  " AND fac_mes = " . ((isset($this->prm->mes) != "")?$this->prm->mes:date("m"));
		$this->conectar();
		$this->setSql('
			SELECT COUNT(*) as tot FROM cat_usuario
		');
		// echo $this->getSql();
		$this->consultar();
		$r = $this->extraer();
		return $r['tot'];
	}
	/* Ventana modal para Usuario
	 * @author: @Robotonio Huazo
	 * @date: 08 Mar 2018
	 */ 
	private function lisUsuario(){
		$h='';
		$a = $this->sqlUsuario();
		$attrs = '';
					$h.='<div class="col-md-10 col-md-offset-1">';
					$h.='<div class="table-responsive">';
						$h.='<table class="table table-striped table-bordered table-collapsed">';
							$h.='<thead>';
								$h.='<tr >';
								$h.='<th>Acci&oacute;n</th>';
								$h.='<th>Usuario</th>';
								$h.='<th>contrase&ntilde;a</th>';
								$h.='<th>Estado</th>';
								$h.='</tr>';
							$h.='</thead>';
							$h.='<tbody id="tbdyUsuario" >';
								foreach( $a as $o ){
									$h.='<tr>';
										$h.='<td>';
											$h.='<a class="btn btn-xs btn-primary edi-use" data-toggle="modal" href="#mdlUsuario" use_id="'.$o->use_id.'">';
												$h.='<i class="fa fa-pencil-square-o"></i>';
											$h.='</a>';
											$h.='<a href="http://localhost/ejercicio/map/tuto3.php?use_id='.$o->use_id.'" class="btn btn-xs btn-info edi-use" data-toggle="modal" href="#mdlUsuario" use_id="'.$o->use_id.'">';
												$h.='<i class="fa fa-print"></i>';
											$h.='</a>';
										$h.='</td>';
										$h.='<td>'.$o->usuario.'</td>';
										$h.='<td>'.$o->password.'</td>';
										$h.='<td>'.$o->estado.'</td>';
									$h.='</tr>';
							}
							$h.='</tbody>';
						$h.='</table>';
					$h.='</div>';
					$h.='</div>';
		$this->mixScript('
			$(".edi-use").click( function(){
				comunica("'.$this->pantalla.'",{metodo:"frmUsuario",use_id:$(this).attr("use_id")});
			});
		');
		return $h;
	}
	/* Ventana Actualizar navPag para Usuario
	 * @author: @Robotonio Huazo
	 * @update: @SirLeoAnt
	 * @date: 08 Mar 2018
	 */ 
	private function nextPag(){
		$this->mixHtmlContenedor( $this->lisUsuario() ,'lisUsuario' );
		$this->mixHtmlContenedor( $this->navPag(),'pag_natn');
	}
	/* Ventana modal para Usuario
	 * @author: @Robotonio Huazo
	 * @date: 08 Mar 2018
	 */ 
	private function main(){
		$h='';
		$h.=$this->mdlUsuario();
		$h.='<h1 align="center"> Lista de Usuarios</h1>';
		$h.='<div clas="row">';
			$h.='<div class="container">';
				$h.='<div class="form-group" style="margin-bottom:42px; margin-top:10px">';
					$h.='<div class="col-xs-4 col-xs-offset-8">';
						$h.='<div class="btn-group pull-right">';
							$h.='<a id="bak" class="btn btn-info" href="#"><i class="fa fa-arrow-left"></i>Regresar</a>';
							$h.='<a class="btn btn-md btn-success" id="add-use" data-toggle="modal" href="#mdlUsuario"><i class="fa fa-plus"></i>Agregar Usuario</a>';
						$h.='</div>';
					$h.='</div>';
				$h.='</div>';
				$h.='<div class="form-group" style="margin-bottom:42px; margin-top:10px">';
					$h.='<div class="col-xs-8 col-xs-offset-2">';
						$h.='<div id="pag_natn" class="form-group">';
							$h.=$this->navPag();
						$h.='</div>';
					$h.='</div>';
				$h.='</div>';
				$h.='<div class="form-group" id="lisUsuario">';
					$h.=$this->lisUsuario();
				$h.='</div>';
			$h.='</div>';
		$h.='</div>';
		$this->mixScript('
			$("#add-use").click( function(){
				comunica("'.$this->pantalla.'",{metodo:"frmUsuario"});
			});
			$("#bak").click( function(){ comunica("'.$this->pantalla.'",{metodo:"main"}); });
		');
	$this->mixHtmlContenedor($h,'dda-content');
	}
}
new cUsuario($parametros);
?>
