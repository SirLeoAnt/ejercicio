<?php
	define('FPDF_FONTPATH','../fnt_old/');
	require_once('fpdf.1.0.0.php');

	class PDF extends FPDF {
		var $widths;
		var $aligns;
		var $borders;
		var $altolinea;
		var $altoline;
		var $intercelda;
		var $espaciado=1.7;
		var $relleno;

		function SetRelleno($f)
		{$this->relleno = $f;
		}
		function SetWidths($w){
			//Set the array of column widths
			$this->widths=$w;
		}
		function SetBorders($b){
			//Set the border
			if($b==1)
				$this->borders='LTBR';
			else
				$this->borders=$b;
		}
		function GetBorders(){
			//Get the border
			return $this->borders;
		}
		function SetAltoLinea($alto){
			//Set the line
			if($alto=='')
				$this->altolinea=2.5;
			else
				$this->altolinea=$alto;
		}
		function GetAltoLinea(){
			//Get the line
			return $this->altolinea;
		}
		function setEspaciado($e){
			$this->espaciado=$e;
		}
		function SetAligns($a){
			//Set the array of column alignments
			$this->aligns=$a;
		}
/*		
		function Row($data){
			//Calculate the height of the row			
			
			$nb=0;
			$cellNb=array();
			$altoline=$this->GetAltoLinea();
			$intercelda=1;
			for($i = 0; $i < count($data); $i++){
				$enebe = $this->NbLines( $this->widths[$i], $data[$i] );
				
				array_push($cellNb,$enebe);
			}
			
			$nb = max($cellNb);
			$h=($altoline*$nb)+$this->espaciado;
			//Issue a page break first if needed
			$this->CheckPageBreak($h);
			//Draw the cells of the row
			$w = $a = $x = $y = '';
			
			for($i=0;$i<count($data);$i++) {
				$w = $this->widths[$i];
				$a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
				//Save the current position
				$x=$this->GetX();
				$y=$this->GetY();
				//Draw the border
				//Print the text
				
				$altura =(($h-$this->espaciado)/$cellNb[$i])+($this->espaciado/$cellNb[$i]);
				
				if($this->relleno == "si")
				{
				$this->MultiCell($w,$altura,$data[$i],$this->borders,$a,true);}
				else{$this->MultiCell($w,$altura,$data[$i],$this->borders,$a);
				}
				//Put the position to the right of the cell
				$this->SetXY($x+$w,$y);
			}
			
			//Go to the next line
			$this->Ln($h);
		}
*/
		
		function Row($data){
			//Calculate the height of the row			
			$nb=0;
			$altolinea=3;
			$intercelda=0;
			for($i = 0; $i < count($data); $i++)
				$nb = max( $nb, $this->NbLines( $this->widths[$i], $data[$i] ) );
			$h=$altolinea*$nb;
			//Issue a page break first if needed
			$this->CheckPageBreak($h);
			//Draw the cells of the row
			$w = $a = $x = $y = '';
			
			for($i=0;$i<count($data);$i++) {
				$w = $this->widths[$i];
				$a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
				//Save the current position
				$x=$this->GetX();
				$y=$this->GetY();
				//Draw the border
				if($this->borders)
					$this->Rect($x,$y,$w,$h);
				//Print the text
				$this->MultiCell($w,$altolinea,$data[$i],0,$a);
				//Put the position to the right of the cell
				$this->SetXY($x+$w,$y);
			}
			
			//Go to the next line
			$this->Ln($h+$intercelda);
		}
		
		function CheckPageBreak($h){
			//If the height h would cause an overflow, add a new page immediately
			if($this->GetY()+$h > $this->PageBreakTrigger)
				{$this->AddPage($this->CurOrientation);
				return true;}
		}
		function NbLines($w,$txt){
			//Computes the number of lines a MultiCell of width w will take
			$cw=&$this->CurrentFont['cw'];
			if($w==0)
				$w=$this->w-$this->rMargin-$this->x;
			$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
			$s=str_replace("\r",'',$txt);
			$nb=strlen($s);
			if($nb>0 and $s[$nb-1]=="\n")
				$nb--;
			$sep=-1;
			$i=0;
			$j=0;
			$l=0;
			$nl=1;
			while($i<$nb){
				$c=$s[$i];
				if($c=="\n"){
					$i++;
					$sep=-1;
					$j=$i;
					$l=0;
					$nl++;
					continue;
				}
				if($c==' ')
					$sep=$i;
				$l+=$cw[$c];
				if($l>$wmax) {
					if($sep==-1){
						if($i==$j)
							$i++;
					}
					else
						$i=$sep+1;
					$sep=-1;
					$j=$i;
					$l=0;
					$nl++;
				}
				else
					$i++;
			}
			return $nl;
		}
		function RoundedRect($x, $y, $w, $h, $r, $style = '', $angle = '1234'){
		
		/*
			x, y: top left corner of the rectangle.
			w, h: width and height.
			r: radius of the rounded corners.
			style: same as Rect(): F, D (default), FD or DF.
			angle: numbers of the corners to be rounded: 1, 2, 3, 4 or any combination (1=top left, 2=top right, 3=bottom right, 4=bottom left).				
		*/
			$k = $this->k;
			$hp = $this->h;
			if($style=='F')
				$op='f';
			elseif($style=='FD' or $style=='DF')
				$op='B';
			else
				$op='S';
			$MyArc = 4/3 * (sqrt(2) - 1);
			$this->_out(sprintf('%.2f %.2f m', ($x+$r)*$k, ($hp-$y)*$k ));

			$xc = $x+$w-$r;
			$yc = $y+$r;
			$this->_out(sprintf('%.2f %.2f l', $xc*$k, ($hp-$y)*$k ));
			if (strpos($angle, '2')===false)
				$this->_out(sprintf('%.2f %.2f l', ($x+$w)*$k, ($hp-$y)*$k ));
			else
				$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

			$xc = $x+$w-$r;
			$yc = $y+$h-$r;
			$this->_out(sprintf('%.2f %.2f l', ($x+$w)*$k, ($hp-$yc)*$k));
			if (strpos($angle, '3')===false)
				$this->_out(sprintf('%.2f %.2f l', ($x+$w)*$k, ($hp-($y+$h))*$k));
			else
				$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

			$xc = $x+$r;
			$yc = $y+$h-$r;
			$this->_out(sprintf('%.2f %.2f l', $xc*$k, ($hp-($y+$h))*$k));
			if (strpos($angle, '4')===false)
				$this->_out(sprintf('%.2f %.2f l', ($x)*$k, ($hp-($y+$h))*$k));
			else
				$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

			$xc = $x+$r ;
			$yc = $y+$r;
			$this->_out(sprintf('%.2f %.2f l', ($x)*$k, ($hp-$yc)*$k ));
			if (strpos($angle, '1')===false)
			{
				$this->_out(sprintf('%.2f %.2f l', ($x)*$k, ($hp-$y)*$k ));
				$this->_out(sprintf('%.2f %.2f l', ($x+$r)*$k, ($hp-$y)*$k ));
			}
			else
				$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
			$this->_out($op);
		}
		function _Arc($x1, $y1, $x2, $y2, $x3, $y3){
			$h = $this->h;
			$this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c ', $x1*$this->k, ($h-$y1)*$this->k, 
				$x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
		}
		
	}
	
?>