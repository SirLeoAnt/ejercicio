<?php
define('FPDF_FONTPATH',dirname(__FILE__).'/../fnt/');
require_once dirname(__FILE__).'/fpdf.1.8.1.php';

class PDF extends FPDF
{
	var $widths;
	var $aligns;
	var $borders;
	var $arrayBorders;
	var $fuentes;
	var $altolinea;
	var $altoline;
	var $intercelda;
	var $espaciado=1.7;
	var $relleno;
	var $topalign;
	var $angle=0;
	
	var $sql='';
	var $conexion='';
	var $cortes='';
	var $datosReporte = null;
	var $cambioCorte = false;
	var $contRegistros = 0;
	
	public function  __construct($orientation = 'P', $unit = 'mm', $size = 'letter', $obj = null)
	{			
		parent::__construct($orientation, $unit, $size, $obj);
	}
	
	function SetTopAlign($f=false)
	{
		$this->topalign=$f;
	}
	function SetRelleno($f)
	{
		$this->relleno = $f;
	}
	function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}
	function SetArrayBorders($w)
	{
		//Set the array of column widths
		$this->arrayBorders=$w;
	}
	function SetBorders($b)
	{
		//Set the border
		if($b==1)
			$this->borders='LTBR';
		else
			$this->borders=$b;
	}
	function GetBorders()
	{
		//Get the border
		return $this->borders;
	}
	function SetAltoLinea($alto)
	{
		//Set the line
		if($alto=='')
			$this->altolinea=2.5;
		else
			$this->altolinea=$alto;
	}
	function GetAltoLinea()
	{
		//Get the line
		return $this->altolinea;
	}
	function setEspaciado($e)
	{
		$this->espaciado=$e;
	}
	function SetAligns($a){
		//Set the array of column alignments
		$this->aligns=$a;
	}
	function setFuentes($a){
		$this->fuentes = $a;
	}
	function Row($data,$border='')
	{
		//CALCULAR EL ALTO DE LAS CELDAS QUE SE DIBUJAN
		
		$nb=0;
		$cellNb=array();
		$altoline=$this->GetAltoLinea();
		$intercelda=1;
		for($i = 0; $i < count($data); $i++){
			$enebe = $this->NbLines( $this->widths[$i], trim($data[$i]) );
			$data[$i] = trim($data[$i]);
			array_push($cellNb,$enebe);
		}
		// ALTURA MAXIMA EXPRESADA EN LINEAS  // THE MAX HEIGTH EXPRESSED ON LINES
		$nb = max($cellNb);
		
		if($this->topalign ==true){
			for($i = 0; $i < count($data); $i++){
				$cellNb[$i]=$nb; //IGUALAMOS TODOS LOS ALTOS PARA QUE SE VEAN PAREJAS LAS CELDAS CONTIGUAS  // ESTABLISH ALL HEIGHTS AS MAXIMUM IS, JUST FOR THE CELLS SHOWS IT AS EQUAL
				$rest = $nb-$this->NbLines( $this->widths[$i], $data[$i] ); //CALCULAMOS CUANTAS LINEAS MENOS TIENE CADA CELDA QUE NO SEA LA MAS GRANDE  // CALCULATE THE NUMBER OF MISSING LINES BUT THE MAX
				$nk='';
				for($h =1;$h<=$rest; $h++){
					//AGREGAMOS SALTOS DE LINEA SIMPLES PARA IGUALAR EL NUMERO DE LINEAS /// ADD THE SIMPLE LINES FOR ALL CELLS BE EQUAL
					$nk.='
					';
				}
				
				///AGREGAMOS LAS LINEAS QUE LE FALTAN A CADA CELDA PARA LLEGAR AL MAXIMO  // ADD THE LINES TO EACH CELL TO REACH THE MAXIMUN HEIGHT OF THE ARRAY
				if(trim($data[$i])<>""){
					$data[$i]=trim($data[$i]).$nk;
				}else{
					$data[$i]=$data[$i].$nk;
				}
			}
			
		}
		$nb = max($cellNb);
		
		$h=($altoline*$nb)+$this->espaciado;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		$w = $a = $x = $y = '';
		$bordeLR = $border == 'LR';
		for ($i=0; $i < count($data); $i++) {
			$borders = 0;
			if ($bordeLR) {
				if ($i == 0) {
					$borders = 'L';
				} elseif ($i == count($data) - 1) {
					$borders = 'R';
				}
			} elseif (!empty($this->arrayBorders)) {
				$borders = $this->arrayBorders[$i];
			} else {
				$borders = $this->borders;
			}
			$w = $this->widths[$i];
			$a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			$valign = false;
			if (strpos($a,"T") !== false) {
				$a = str_replace("T", '', $a);				
				$valign = 'T';
			} elseif (strpos($a,"B") !== false) {
				$a = str_replace("B", '', $a);
				$valign = 'B';
			}
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			//Print the text
			$altura =(($h-$this->espaciado)/$cellNb[$i])+($this->espaciado/$cellNb[$i]);		
			if (!empty($this->fuentes)) {
				$this->SetFont(
					$this->fuentes[$i]->font,
					$this->fuentes[$i]->style,
					$this->fuentes[$i]->size)
				;
			}
			
			if ($this->relleno == "si") {
				if ($valign!==false) {
					$this->drawTextBox($data[$i],$w,$altura,$a,$valign,$borders,true);
				} else {
					$this->MultiCell($w,$altura,$data[$i],$borders,$a,true);					
				}
			} else {
				if ($valign!==false) {
					$this->drawTextBox($data[$i],$w,$altura,$a,$valign,$borders);
				} else {
					$this->MultiCell($w,$altura,$data[$i],$borders,$a);
				}
			}
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		$this->fuentes =  NULL;
		//Go to the next line
		$this->Ln($h);
	}
	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h > $this->PageBreakTrigger)
			{$this->AddPage($this->CurOrientation);
			return true;}
	}
	function NbLines($w,$txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb){
			$c=$s[$i];
			if($c=="\n"){
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax) {
				if($sep==-1){
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
	function RoundedRect($x, $y, $w, $h, $r, $style = '', $angle = '1234')
	{
	
	/*
		x, y: top left corner of the rectangle.
		w, h: width and height.
		r: radius of the rounded corners.
		style: same as Rect(): F, D (default), FD or DF.
		angle: numbers of the corners to be rounded: 1, 2, 3, 4 or any combination (1=top left, 2=top right, 3=bottom right, 4=bottom left).				
	*/
		$k = $this->k;
		$hp = $this->h;
		if($style=='F')
			$op='f';
		elseif($style=='FD' or $style=='DF')
			$op='B';
		else
			$op='S';
		$MyArc = 4/3 * (sqrt(2) - 1);
		$this->_out(sprintf('%.2f %.2f m', ($x+$r)*$k, ($hp-$y)*$k ));

		$xc = $x+$w-$r;
		$yc = $y+$r;
		$this->_out(sprintf('%.2f %.2f l', $xc*$k, ($hp-$y)*$k ));
		if (strpos($angle, '2')===false)
			$this->_out(sprintf('%.2f %.2f l', ($x+$w)*$k, ($hp-$y)*$k ));
		else
			$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

		$xc = $x+$w-$r;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2f %.2f l', ($x+$w)*$k, ($hp-$yc)*$k));
		if (strpos($angle, '3')===false)
			$this->_out(sprintf('%.2f %.2f l', ($x+$w)*$k, ($hp-($y+$h))*$k));
		else
			$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

		$xc = $x+$r;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2f %.2f l', $xc*$k, ($hp-($y+$h))*$k));
		if (strpos($angle, '4')===false)
			$this->_out(sprintf('%.2f %.2f l', ($x)*$k, ($hp-($y+$h))*$k));
		else
			$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

		$xc = $x+$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2f %.2f l', ($x)*$k, ($hp-$yc)*$k ));
		if (strpos($angle, '1')===false)
		{
			$this->_out(sprintf('%.2f %.2f l', ($x)*$k, ($hp-$y)*$k ));
			$this->_out(sprintf('%.2f %.2f l', ($x+$r)*$k, ($hp-$y)*$k ));
		}
		else
			$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
		$this->_out($op);
	}
	function _Arc($x1, $y1, $x2, $y2, $x3, $y3){
		$h = $this->h;
		$this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c ', $x1*$this->k, ($h-$y1)*$this->k, 
			$x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
	}
	function Rotate($angle,$x=-1,$y=-1){
		if($x==-1)
			$x=$this->x;
		if($y==-1)
			$y=$this->y;
		if($this->angle!=0)
			$this->_out('Q');
		$this->angle=$angle;
		if($angle!=0)
		{
			$angle*=M_PI/180;
			$c=cos($angle);
			$s=sin($angle);
			$cx=$x*$this->k;
			$cy=($this->h-$y)*$this->k;
			$this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
		}
	}
	function _endpage(){
		if($this->angle!=0)
		{
			$this->angle=0;
			$this->_out('Q');
		}
		parent::_endpage();
	}
	function RotatedText($x, $y, $txt, $angle){
		//Text rotated around its origin
		$this->Rotate($angle,$x,$y);
		$this->Text($x,$y,$txt);
		$this->Rotate(0);
	}

	function setSql($consulta){ $this->sql = $consulta; }
	
	function pintaReporte($reporte){
		$this->conexion = new AccesoDatos();			
		$this->conexion->conectar();
		$this->conexion->setSql($this->sql);
		$this->conexion->consultar();
		$this->conexion->desconectar();
		$row = $this->conexion->extraer();
		//$this->datos contiene el primer registro de la consulta
		$this->setDatos($row);
		$this->setAltoLinea(1.5);
		$this->AddPage();

		foreach($reporte->cortes->datos as $datos){
			$this->cortes[ $datos->campoId ] = '';
		}
		$this->datosReporte = array();
		$this->cambioCorte = false;
		$montosGlobal = new stdClass();
		foreach( $reporte->montos->campos as $campo )
				$montosGlobal->$campo = 0;
		if( $this->conexion->getNumeroRegistros() > 0){
			do{
				$this->setDatos($row);
				$d = $this->objetoCorte(0,$reporte->cortes,$reporte->montos->campos,$reporte->detalle->campos);
				array_push($this->datosReporte,$d);
				foreach( $reporte->montos->campos as $campo ){
					$montosGlobal->$campo += $d->montos->$campo;
				}
				if( $this->contRegistros < $this->conexion->getNumeroRegistros()){
					mysqli_data_seek($this->conexion->getConsultaId(), $this->contRegistros);
					$this->contRegistros++;
				}
			}while( $row = $this->conexion->extraer() );
			$primeraVez = true;
			if( isset($reporte->importeGlobal) ){
				if( isset($reporte->importeGlobal->font) )
					$this->SetFont( $reporte->importeGlobal->font[0], $reporte->importeGlobal->font[1], $reporte->importeGlobal->font[2] );
				else
					$this->SetFont('Arial','B',6);
				$aligns = ( isset($reporte->importeGlobal->align) ) ? array($reporte->importeGlobal->align)  :array('C');
				$width = 0;
				foreach( $reporte->cortes->width as $k)
					$width += $k;
				$width = array($width);
				foreach( $reporte->montos->width as $k){
					array_push($aligns,'R');
				}
				$width = array_merge($width, $reporte->montos->width);
				$fila = array( $reporte->importeGlobal->descripcion );
				foreach( $montosGlobal as $monto){
					array_push($fila,$monto);
				}
				$this->SetWidths( $width );
				$this->SetAligns( $aligns );
				$this->SetFillColor(255,255,255);
				if($reporte->importeGlobal->posicion == 'principio'){
					$this->Row($fila);
					$this->ln(2);
				}
			}
			foreach( $this->datosReporte as $datos){
				if( !$primeraVez )$this->ln(4);
				$this->pintaCorte($datos,$reporte);
				$primeraVez = false;
			}			
			if(isset($reporte->importeGlobal) && $reporte->importeGlobal->posicion == 'final'){
				$this->ln(5);
				if( isset($reporte->importeGlobal->font) )
					$this->SetFont( $reporte->importeGlobal->font[0], $reporte->importeGlobal->font[1], $reporte->importeGlobal->font[2] );
				else
					$this->SetFont('Arial','B',6);
				$this->SetWidths( $width );
				$this->SetAligns( $aligns );
				$this->SetFillColor(255,255,255);
				$this->Row($fila);
			}
		}
		echo "<code>".json_encode( $this->datosReporte )."'</code>";
		die();
		return $montosGlobal;
	}
	
	function objetoCorte( $nivel,$corte,$camposMontos,$camposDetalle ){
		$o = new stdClass();
		$o->montos = new stdClass();
		if( isset($corte->datos[$nivel]) && isset($this->cortes[ $corte->datos[$nivel]->campoId ]) ){
			$o->descripcion = utf8_decode( $corte->datos[$nivel]->descripcion );
			$o->nombre = utf8_decode( $this->datos[ $corte->datos[$nivel]->campoNombre ] );
			$o->valor = utf8_decode( $this->datos[ $corte->datos[$nivel]->campoId ] );
			// se inicializan los montos del nivel
			foreach( $camposMontos as $campo )
				$o->montos->$campo = 0;
			$this->cortes[$corte->datos[$nivel]->campoId] = $this->datos[ $corte->datos[$nivel]->campoId ];
			$o->sigNivel = array();
			do{
				if( isset($corte->datos[ $nivel+1 ]) && isset($this->cortes[ $corte->datos[ $nivel+1 ]->campoId ]) ){
					$d = $this->objetoCorte($nivel+1,$corte,$camposMontos,$camposDetalle);
					array_push($o->sigNivel,$d);
				}else{
					if ( is_object($camposDetalle) ){
						foreach($camposDetalle->filas as $filas){
							$d = $this->objetoCorte($nivel+1,$corte,$camposMontos,$filas);
							array_push($o->sigNivel,$d);
						}
					}else{
						$d = $this->objetoCorte($nivel+1,$corte,$camposMontos,$camposDetalle);
						array_push($o->sigNivel,$d);
					}
				}
				foreach( $camposMontos as $campo ){
					$o->montos->$campo += $d->montos->$campo;
				}
				if(!$this->cambioCorte){
					if( $this->datos = $this->conexion->extraer() ){
						$this->contRegistros++;
						for( $x = 0; $x<=$nivel; $x++ ){						
							if( $this->datos[ $corte->datos[$x]->campoId ] != $this->cortes[$corte->datos[$x]->campoId] ){
								$this->cambioCorte = true;
								break;
							}
						}
					}else{
						$this->contRegistros++;
						// $this->cambioCorte=true;
						break;
					}
				}
				// echo $this->cambioCorte.' - '.$this->contRegistros.' < '.$this->conexion->getNumeroRegistros().' |-_-| ';
			}while( !$this->cambioCorte );
		}else{
			// print_r($camposDetalle); die();
			foreach( $camposDetalle as $campo ){
				$valor = '';
				$nombreCampo = '';
				if( is_object($campo) ){
							//print_r($this->datos[$campo->campo].'|');	
					switch($campo->formato){
						case 'entero': $valor = number_format( $this->datos[$campo->campo]); 
							//print_r($valor.'|');	
						break;
						case 'moneda': $valor = number_format( $this->datos[$campo->campo]); break;
						case 'texto':default:
							$valor = utf8_decode($this->datos[$campo->campo]);
						break;
					}
					$nombreCampo = $campo->campo;
				}else{
					//print_r($campo->campo.'|');	
					$valor = utf8_decode($this->datos[ $campo ]);
					$nombreCampo = $campo;
				}
				$o->$nombreCampo = $valor;
			}
			
			foreach( $camposMontos as $campo )
				$o->montos->$campo = $this->datos[ $campo ];
		}
		return $o;
		
	}

	function pintaCorte($dato,$reporte,$t=8){
		$t = ( $t == 6 )?6 :$t - 1;
		$width = isset($reporte->montos) ? array_merge($reporte->cortes->width, $reporte->montos->width) :$reporte->cortes->width;
		//$width = array_merge($reporte->cortes->width, $reporte->montos->width);
		$aligns = array('L','L');
		foreach( $reporte->montos->width as $k){
			array_push($aligns,'R');
		}
		$fila = array( $dato->descripcion.': '.$dato->valor,$dato->nombre );
		foreach( $dato->montos as $v)
			array_push( $fila, number_format($v,2) );
		$this->SetX(10);
		$this->SetFont('Arial','B',$t); ///tipo de letra para cada titulo
		$this->SetRelleno("no"); 
		$this->setBorders('B');
		$this->setAltoLinea(2);
		$this->setDrawColor(160,160,160);
		$this->SetFillColor(192,192,192);
		$this->SetWidths( $width );
		$this->SetAligns($aligns);
		$this->Row($fila);
		if( isset($dato->sigNivel[0]->sigNivel) )
			foreach( $dato->sigNivel as $nextN)
				$this->pintaCorte($nextN,$reporte,$t);
		else{
			$this->ln(2);
			// $this->setX(10);
			$this->setBorders(0);
			$this->SetFillColor(199,209,219);
			$width = $reporte->detalle->width;
			$aligns = $reporte->detalle->aligns;
			$widthTotal = 0;
			foreach( $width as $w){
				$widthTotal += $w;
			}
			foreach( $reporte->montos->width as $v){
				array_push($aligns,'R');
				array_push($width,$v-3);
			}
			$this->RoundedRect(10, $this->GetY()-.5,$widthTotal, 4, 1, 'DF', '1234');		
			if( isset($reporte->detalle->font) )
				$this->SetFont( $reporte->detalle->font[0], $reporte->detalle->font[1], $reporte->detalle->font[2] );
			else
				$this->SetFont('Arial','B',6);
			$this->SetWidths( $width );
			$this->SetAligns( $aligns );
			$this->Row($reporte->detalle->cabecera);
			$this->SetFillColor(255,255,255);
			$this->ln(2);
			$this->setBorders(0);
			$this->SetRelleno("no");
			foreach( $dato->sigNivel as $nextN){
				$d = array();
				foreach( $nextN as $campo=>$detalle){
					if( $campo !='montos' )
					array_push($d, utf8_decode($detalle) );
				}
				foreach( $nextN->montos as $v)
					array_push( $d, number_format($v,2) );
				$this->setBorders('B');
				$this->setDrawColor(240,240,240);
				$this->Row($d);
				$this->setBorders(0);
			}
				// $this->pintaDetalle($nextN,$reporte,$t);
		}
	}

	/**
	 * Draws text within a box defined by width = w, height = h, and aligns
	 * the text vertically within the box ($valign = M/B/T for middle, bottom, or top)
	 * Also, aligns the text horizontally ($align = L/C/R/J for left, centered, right or justified)
	 * drawTextBox uses drawRows
	 *
	 * This function is provided by TUFaT.com
	 * https://stackoverflow.com/questions/13069101/pdf-alignment-top-and-bottom
	 */
	function drawTextBox($strText, $w, $h, $align='L', $valign='T', $border=true,$fill=false)
	{
	    $xi=$this->GetX();
	    $yi=$this->GetY();
	    
	    $hrow=$this->FontSize;
	    $textrows=$this->drawRows($w,$hrow,$strText,0,$align,0,0,0);
	    $maxrows=floor($h/$this->FontSize);
	    $rows=min($textrows,$maxrows);

	    $dy=0;
	    if (strtoupper($valign)=='M')
	        $dy=($h-$rows*$this->FontSize)/2;
	    if (strtoupper($valign)=='B')
	        $dy=$h-$rows*$this->FontSize;

	    $this->SetY($yi+$dy);
	    $this->SetX($xi);

	    $this->drawRows($w,$hrow,$strText,0,$align,$fill,$rows,1);

	    if ($border)
	        $this->Rect($xi,$yi,$w,$h);
	}

	function drawRows($w, $h, $txt, $border=0, $align='J', $fill=false, $maxline=0, $prn=0)
	{
	    $cw=&$this->CurrentFont['cw'];
	    if($w==0)
	        $w=$this->w-$this->rMargin-$this->x;
	    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
	    $s=str_replace("\r",'',$txt);
	    $nb=strlen($s);
	    if($nb>0 && $s[$nb-1]=="\n")
	        $nb--;
	    $b=0;
	    if($border)
	    {
	        if($border==1)
	        {
	            $border='LTRB';
	            $b='LRT';
	            $b2='LR';
	        }
	        else
	        {
	            $b2='';
	            if(is_int(strpos($border,'L')))
	                $b2.='L';
	            if(is_int(strpos($border,'R')))
	                $b2.='R';
	            $b=is_int(strpos($border,'T')) ? $b2.'T' : $b2;
	        }
	    }
	    $sep=-1;
	    $i=0;
	    $j=0;
	    $l=0;
	    $ns=0;
	    $nl=1;
	    while($i<$nb)
	    {
	        //Get next character
	        $c=$s[$i];
	        if($c=="\n")
	        {
	            //Explicit line break
	            if($this->ws>0)
	            {
	                $this->ws=0;
	                if ($prn==1) $this->_out('0 Tw');
	            }
	            if ($prn==1) {
	                $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
	            }
	            $i++;
	            $sep=-1;
	            $j=$i;
	            $l=0;
	            $ns=0;
	            $nl++;
	            if($border && $nl==2)
	                $b=$b2;
	            if ( $maxline && $nl > $maxline )
	                return substr($s,$i);
	            continue;
	        }
	        if($c==' ')
	        {
	            $sep=$i;
	            $ls=$l;
	            $ns++;
	        }
	        $l+=$cw[$c];
	        if($l>$wmax)
	        {
	            //Automatic line break
	            if($sep==-1)
	            {
	                if($i==$j)
	                    $i++;
	                if($this->ws>0)
	                {
	                    $this->ws=0;
	                    if ($prn==1) $this->_out('0 Tw');
	                }
	                if ($prn==1) {
	                    $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
	                }
	            }
	            else
	            {
	                if($align=='J')
	                {
	                    $this->ws=($ns>1) ? ($wmax-$ls)/1000*$this->FontSize/($ns-1) : 0;
	                    if ($prn==1) $this->_out(sprintf('%.3F Tw',$this->ws*$this->k));
	                }
	                if ($prn==1){
	                    $this->Cell($w,$h,substr($s,$j,$sep-$j),$b,2,$align,$fill);
	                }
	                $i=$sep+1;
	            }
	            $sep=-1;
	            $j=$i;
	            $l=0;
	            $ns=0;
	            $nl++;
	            if($border && $nl==2)
	                $b=$b2;
	            if ( $maxline && $nl > $maxline )
	                return substr($s,$i);
	        }
	        else
	            $i++;
	    }
	    //Last chunk
	    if($this->ws>0)
	    {
	        $this->ws=0;
	        if ($prn==1) $this->_out('0 Tw');
	    }
	    if($border && is_int(strpos($border,'B')))
	        $b.='B';
	    if ($prn==1) {
	        $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
	    }
	    $this->x=$this->lMargin;
	    return $nl;
	}
}
?>