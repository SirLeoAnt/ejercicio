/**
 * Number.prototype.format(n, x, s, c)
 * 
 * @param integer n: length of decimal
 * @param integer x: length of whole part
 * @param mixed   s: sections delimiter
 * @param mixed   c: decimal delimiter
 */
Number.prototype.format = function(n, x, s, c) {
	var re = "\\d(?=(\\d{" + (x || 3) + "})+" + (n > 0 ? "\\D" : "$") + ")",
		num = this.toFixed(Math.max(0, ~~n));

	return (c ? num.replace(".", c) : num).replace(new RegExp(re, "g"), "$&" + (s || ","));
};

/** @param d identifica quien detonó el evento 
 * 		0 = tema
 * 		1 = geo ( juris, mun, clues )
 * 		2 = lis.pet
 *		4 = paginación 
 **/
function resultPet(d){
	// var cont = [{c:"pet-temas",t:"spin"},{c:"pet-geo",t:"spin"},{c:"lis-pet",t:"spin"},{c:"nada",t:"spin"}];
	// cont.splice(d,1); // extrae el contenedor del elemento que está detonando el evento
	// console.log(cont);
	if (d!=0 & d!=4) comunica("mnt/CPrevia.php",{ metodo:"lisCuentaTemas", filters: $("#filters").data(), q:$("#q").val(),estatus:$("#estatus").attr("estatus") },[{c:"pet-temas",t:"spin"}]);	
	if (d!=1 & d!=4) comunica("mnt/CPrevia.php",{ metodo:"lisUbicacion", filters: $("#filters").data(),q:$("#q").val(),estatus:$("#estatus").attr("estatus") },[{c:"pet-geo",t:"circle"}]);	
	if (d!=2) comunica("mnt/CPrevia.php",{ metodo:"lisPeticiones", filters: $("#filters").data(),q:$("#q").val(),estatus:$("#estatus").attr("estatus") },[{c:"lis-pet",t:"spin"}]);	
}
/** Devuelve Iconos **/
function resIco(r){
	$markup = '';
	$markup+='<div class="container-fluid">';
		$markup+='<div class="row">';
			$markup+='<div class="col-xs-8" ><i class="fa fa-' + r.icono+ '" ></i> ' + r.icono+ '</div>';
			$markup+='<div class="col-xs-2" ><span class="badge">' + r.keyWord+ '</span></div>';
		$markup+='</div>';
	$markup+='</div>';
	
	return $markup;
}

Date.dateDiff = function(datepart, fromdate, todate) {	
  datepart = datepart.toLowerCase();	
  var diff = todate - fromdate;	
  var divideBy = { y:31557600000,
				   w:604800000, 
				   d:86400000, 
				   h:3600000, 
				   n:60000, 
				   s:1000 };	
  
  return Math.floor( diff/divideBy[datepart]);
}
function ico(r){
	$ic = '';
	if (r.text) { 
		console.log(r);
		if (r.text == "Selecciona un icono"){				
			return 'Selecciona un icono';
		}else{
			var str = (r.text);
			var res = str.toLowerCase();
			return '<div class="container-fluid"><div class="row"><div style="overflow:hidden;" class="col-xs-12"><span icon="'+ r.text +'" class="badge alert-success icxn"><i class="fa fa-'+ res +'"></i> '+ r.text +'</span></div></div></div>';
		}
	
	// return r.text; 
	}	
	
	$markup = '';
	
	$markup+='<div class="container-fluid">';
		$markup+='<div class="row">';
			$markup += '<div class="col-xs-12" style="overflow:hidden;">';
				$markup += '<span class="badge alert-success icxn" icon="'+ r.icon +'"><i class="fa fa-' + r.icon+ '" ></i> ' + r.icon+ '</span>';
				$markup+='<strong>' + r.descrip + '</strong>';
			$markup+='</div>';
		$markup+='</div>';
	$markup+='</div>';	
	
	return $markup;
}

/** Devuelve Iconos **/
function resIco(r){
	$markup = '';
	
	$markup+='<div class="container-fluid">';
		$markup+='<div class="row">';
			$markup+='<div class="col-xs-8" ><i class="fa fa-' + r.icono+ '" ></i> ' + r.icono+ '</div>';
			$markup+='<div class="col-xs-2" ><span class="badge">' + r.keyWord+ '</span></div>';
		$markup+='</div>';
	$markup+='</div>';
	
	return $markup;
}


/** Devuelve Firmantes **/
function resFirmante(r){
	$markup = '';
	console.log(r);
	if (r.id == "0") return "Escriba un Firmante";
	return markup ='<div class="container-fluid"><div class="row"><div class="col-xs-12"><strong>' + r.nom+ '</strong></div></div></div>';
		
}

function fmtFirBusR(r) {
	// console.log("fmtFirmanteR");
	// console.log(r);
	if (r.loading) return r.text;
	if (r.id == "0") return "Teclee un nombre";
	//console.log("result");
	$m ='';
	$m+='<div class="clearfix">';
	$m+='<div class="col-xs-11">' + r.nom;
		if ( r.sex == 1 )
			$m+=' <i class="fa fa-male"></i> - ';
		else 
			$m+=' <i class="fa fa-female"></i> - ';
		
		switch ( r.t ){
			case 1: $m+='<span class="badge alert-info"><i class="fa fa-user-md"></i></span> '+ r.area; break;
			case 2: $m+='<span class="badge alert-info"><i class="fa fa-user-md"></i></span> '+ r.area; break;
			case 3: $m+='<span class="badge alert-danger"><i class="fa fa-university"></i></span> '+ r.mun; break;
			case 4: $m+='<span class="badge alert-danger"><i class="fa fa-university"></i></span> '; break;
			case 5: $m+='<span class="badge alert-warning"><i class="fa fa-h-square"></i></span> ' + r.clues + ' ' + r.area; break;
		}
	$m+="</div>"
	return $m;
	}
function fmtFirBusS(r) {
	// console.log("fmtFirmanteS");
	console.log("select" );
	console.log( r);
	if (r.id == "0") return "Escriba un nombre";
	if ( r.id.substring(0, 1)=="A" ){
		alert("de de alta el firmante");
		return;
	}
	$m ='';
	$m+='<div class="row">';
		$m+='<div class="col-xs-11">';
			$m+= r.nom;
		$m+='</div>';
	$m+='</div>';	
	
	//$("s2_" + r.title ).empty();
	/*var idTitle = '"'+r.title+'"';
	$("#s2_" + r.title ).html('<div class="form-group"><div class="row"><div class="col-xs-12"><a area_id="' + r.area_id + '" fir_id="' + r.id + '" href="#modal_firmantes" data-toggle="modal" class="btn btn-xs"><i class="fa fa-pencil-square-o"></i></a><a onclick="btnBFir(this)" title='+idTitle+' class="btn btn-xs"><i class="fa fa-eraser"></i></a><label>' + r.nom + '</label></div></div><div class="row"><div class="col-xs-12"><span class="text-info"> &nbsp;&nbsp;' + r.cargo + ' &mdash; ' + r.area + '</span></div></div></div>');*/
	return $m;
	}
function fmtFirmanteR(r) {
	if (r.loading) return r.text;
	if (r.id == "0") return "Teclee un nombre";
	//console.log(r);
	$m ='';
	$m+='<div class="clearfix">';
	$m+='<div class="col-xs-11">' + r.nom;
		if ( r.sex == 1 )
			$m+=' <i class="fa fa-male"></i> - ';
		else 
			$m+=' <i class="fa fa-female"></i> - ';
		
		switch ( r.t ){
			case 1: $m+='<span class="badge alert-info"><i class="fa fa-user-md"></i></span> '+ r.area; break;
			case 2: $m+='<span class="badge alert-info"><i class="fa fa-user-md\"></i></span> '+ r.area; break;
			case 3: $m+='<span class="badge alert-danger"><i class="fa fa-university"></i></span> '+ r.mun; break;
			case 4: $m+='<span class="badge alert-danger"><i class="fa fa-university"></i></span> '; break;
			case 5: $m+='<span class="badge alert-warning"><i class="fa fa-h-square"></i></span> ' + r.clues + ' ' + r.area; break;
		}
	$m+='</div>';
	return $m;
}
function fmtFirmanteS(r) {
	// console.log("fmtFirmanteS");
	console.log(r);
	//console.log(r.title);
	if (r.id == "0") return "Escriba un nombre";
	if ( r.id.substring(0, 1)=="A" ){
		alert("de de alta el firmante");
		return;
	}
	$('s2_' + r.title ).empty();
	var idTitle = '"'+r.title+'"';
	$("#s2_" + r.title ).html('<div class="form-group"><div class="row"><div class="col-xs-12"><a area_id="' + r.area_id + '" fir_id="' + r.id + '" href="#modal_firmantes" data-toggle="modal" class="btn btn-xs"><i class="fa fa-pencil-square-o"></i></a><a onclick="btnBFir(this)" title='+idTitle+' class="btn btn-xs"><i class="fa fa-eraser"></i></a><label>' + r.nom + '</label></div></div><div class="row"><div class="col-xs-12"><span class="text-info"> &nbsp;&nbsp;' + r.cargo + ' &mdash; ' + r.area + '</span></div></div></div>');
	return;
}
function fmtFirmanteRecS(r) {
	if (r.id == "0") return "Escriba un nombre";
	if ( r.id.substring(0, 1)=="A" ){
		alert("de de alta el firmante");
		return;
	}
	$('s2_' + r.title ).empty();
	var idTitle = '"'+r.title+'"';
	$("#s2_" + r.title ).html('<div class="form-group"><div class="row"><div class="col-xs-12"><a area_id="' + r.area_id + '" fir_id="' + r.id + '" href="#modal_firmantes" data-toggle="modal" class="btn btn-xs"><i class="fa fa-pencil-square-o"></i></a><a onclick="btnBFir(this)" title='+idTitle+' class="btn btn-xs"><i class="fa fa-eraser"></i></a><label>' + r.nom + '</label></div></div><div class="row"><div class="col-xs-12"><span class="text-info"> &nbsp;&nbsp;' + r.cargo + ' &mdash; ' + r.area + '</span></div></div></div>');
	if( r.title == 'rem' ){
		console.log( comunica("adc/cRecibir.php",{ 
			metodo:"ugeo",
			fir_id:r.id
		}));
	}
	return;
}
/** Devuelve Areas **/
function devArea(r){
	$markup = '';
	console.log(r);
	$markup+='<div class="container-fluid">';
		$markup+='<div class="row">';
			// $markup+='<div class="col-xs-8" ><i class="fa fa-' + r.icono+ '" ></i> ' + r.icono+ '</div>';
			$markup+='<div class="col-xs-12" ><span class="badge alert-success">' + r.abrev+ '</span> '+ r.nombre +'</div>';
		$markup+='</div>';
	$markup+='</div>';
	
	return $markup;
}

function fmtOtemaS(r){
	if (r.id == "0") return "Escriba un tema...";
	if (r.id == "1"){
		var ouk = r.title.split(",");
		r.id = ouk[0];
		r.abrev = ouk[1];
		r.nombre = ouk[2];
		r.areaAbrev = ouk[3];
	}
	
	return markup ='<div class="container-fluid"><div class="row"><div class="col-xs-11" tema_id="' + r.id+ '" ><strong>' + r.abrev+ '</strong> ' + r.nombre+ '<span class="badge">' + r.areaAbrev+ '</span></div></div></div>';
}
function fmtOtemaR(r){
	if (r.id == "0") return "Escriba un tema";
	return markup ='<div class="container-fluid"><div class="row"><div class="col-xs-11"><strong>' + r.abrev+ '</strong> ' + r.nombre+ '<span class="badge">' + r.areaAbrev+ '</span></div></div></div>';
}

function fmtProveedoresS(r){
	if (r.id == "0") return "B&uacute;sque un proveedor...";
	if (r.id == "1"){
		var ouk = r.title.split(",");
		r.id = ouk[0];
		r.nombre = ouk[1];
	}
	
	return markup ='<div class="container-fluid"><div class="row"><div class="col-xs-11" rfc_id="' + r.id+ '" ><strong>' + r.id+ '</strong> ' + r.nombre+ '</div></div></div>';
}
function fmtProveedoresR(r){
	if (r.id == "0") return "B&uacute;sque un proveedor...";
	if (r.id == "1"){
		var ouk = r.title.split(",");
		r.id = ouk[0];
		r.nombre = ouk[1];
	}
	
	return markup ='<div class="container-fluid"><div class="row"><div class="col-xs-11" rfc_id="' + r.id+ '" ><strong>' + r.id+ '</strong> ' + r.nombre+ '</div></div></div>';
}
function fmtPartidaR(r){
	if (r.id == "0") return "Escriba la clave presupuestal...";
	if (r.id == "1"){
		var ouk = r.title.split(",");
		r.id = ouk[0];
		r.partida = ouk[1];
	}
	
	return markup ='<div class="container-fluid"><div class="row"><div class="col-xs-11" pre_clave="' + r.id+ '" ><strong>' + r.id+ '</strong> ' + r.partida+ '</div></div></div>';
}
function fmtPartidaS(r){
	if (r.id == "0") return "Escriba la clave presupuestal...";
	if (r.id == "1"){
		var ouk = r.title.split(",");
		r.id = ouk[0];
		r.partida = ouk[1];
	}
	
	return markup ='<div class="container-fluid"><div class="row"><input id="pe_id" type="hidden" value="'+r.pe_id+'"><input id="pg_id" type="hidden" value="'+r.pg_id+'"><div class="col-xs-11" pre_clave="' + r.id+ '" ><strong>' + r.id+ '</strong></div></div></div>';
}
function conceptoDam(r){
	$markup = '';
	$markup+='<div class="container-fluid">';  
		$markup+='<div class="row" style="height:25px">';
			$markup+='<div class="col-xs-10"><i style="color:#371591" >' + r.clave+ '</i> - '+ r.concepto+'</div>';			
			$markup+='<div class="col-xs-2" ><span class="badge" data-toggle="tooltip" title="' + r.damnombre+ '">' + r.damabrev+ '</span></div>';
		$markup+='</div>';
	$markup+='</div>';
	
	return $markup;
}

function fmtMunicipioR(r){

	console.log("seleccion");
	
	if (r.id == "0") return "Escriba un municipio...";
	if (r.id == "1"){
		var ouk = r.title.split(",");
		r.id = ouk[0];
		r.abrev = ouk[1];
		r.nombre = ouk[2];
		r.areaAbrev = ouk[3];
	}
	
	markup = '';
	if (r.jur_u == 1){ 
		$csspmi = 's_riis';		
	} else { 
		$csspmi = 'n_riis'; 		
	}
	
	markup +='<div class="row top10 ' + $csspmi + '" mun_id="'+r.mun_id+'">';
		markup +='<div class="col-xs-12">';
			markup+='<span class="bm_jur">J-'+ r.jur +'</span>';
			markup+='<span class="bm_mpo"><i class="fa fa-university"></i> '+ r.id +' </span>';		
			markup+='<span class="bm_mun"> '+ r.nombre +'</span>';		
		markup+='</div>';				
	markup+='</div>';	
	
	
	return markup; 
	
}
function fmtMunicipioS(r){
	if (r.id == "0") return "Escriba un municipio...";
	if (r.id == "1"){
		var ouk = r.title.split(",");
		r.id = ouk[0];
		r.abrev = ouk[1];
		r.nombre = ouk[2];
		r.areaAbrev = ouk[3];
	}
	
	markup = '';
	if (r.jur_u == 1) $csspmi = 's_riis'; else $csspmi = 'n_riis';
	
	markup +='<div class="row top10 ' + $csspmi + '" mun_id="'+r.mun_id+'">';
		markup +='<div class="col-xs-12" >';
			markup+='<span class="bn_mpo"><i class="fa fa-university "></i> '+ r.id +' </span>';		
			markup+='<span class="bn_mun">  '+ r.nombre +'</span>';		
		markup+='</div>';				
	markup+='</div>';	
	
	
	return markup;
}

function fmtLocalidadR(r){

	console.log("OSEA");
	
	if (r.id == "0") return "Escriba una clues...";
	if (r.id == "1"){
		var ouk = r.title.split(",");
		r.id = ouk[0];
		r.abrev = ouk[1];
		r.nombre = ouk[2];
		r.areaAbrev = ouk[3];
	}
	
	markup = '';
	
	markup +='<div class="row">';
		markup +='<div class="col-xs-10">';
			// markup+='<span class="badge alert-success"><label>' + r.id + '</label></span>';		
			markup+='<label>' + r.localidad + '</label>';
		markup+='</div>';				
	markup+='</div>';	
	
	
	return markup; 
	
}
function fmtLocalidadS(r){
	console.log("Selecciona");
	
	if (r.id == "0") return "Escriba una clues...";
	if (r.id == "1"){
		var ouk = r.title.split(",");
		r.id = ouk[0];
		r.abrev = ouk[1];
		r.nombre = ouk[2];
		r.areaAbrev = ouk[3];
	}
	
	markup = '';
	
	markup +='<div class="row cluid" clues_id="'+r.clues_id+'">';
		markup +='<div class="col-xs-10">';			
			markup+='<label>' + r.localidad + '</label>';
		markup+='</div>';
	markup+='</div>';
	
	return markup;
}

function fmtCluesPMIR(r){
	//console.log(r);
	
	if (r.id == "0") return "Escriba una clues...";
	if (r.id == "1"){
		var ouk = r.title.split(",");
		r.id = ouk[0];
		r.abrev = ouk[1];
		r.nombre = ouk[2];
		r.areaAbrev = ouk[3];
	}
	
	markup = '';
	
	markup +='<div class="row">';
		markup +='<div class="col-xs-10">';
			markup+='<span class="badge alert-success"><label>' + r.id + '</label></span>';		
			markup+=' <span class="badge alert-danger">' + r.tpg_id + '</span>';
			markup+=' <label>' + r.nombre + '</label>';
		markup+='</div>';	
			if (r.pmi == 1){
				markup +='<div class="col-xs-1">';
					markup+='<span class="badge alert-info">PMI <i class="fa fa-gavel" aria-hidden="true"></i></span>';
				markup +='</div>';	
			}		
	markup+='</div>';
	
	markup+='<div class="row">';
		markup +='<div class="col-xs-10">';
			markup+='<a><i class="fa fa-university text-danger" aria-hidden="true" ></i></a>';
			markup+='<label>' + r.localidad + ', ' + r.municipio + '</label>';
		markup +='</div>';	
	
	markup+='</div>';
	
	return markup; 
}
function fmtCluesPMIS(r){
	console.log("Selecciona");
	
	if (r.id == "0") return "Escriba una clues...";
	if (r.id == "1"){
		var ouk = r.title.split(",");
		r.id = ouk[0];
		r.abrev = ouk[1];
		r.nombre = ouk[2];
		r.areaAbrev = ouk[3];
	}
	
	markup = '';
	
	markup +='<div class="row cluid" clues_id="'+r.clues_id+'">';
		markup +='<div class="col-xs-10">';
			markup+='<span class="badge alert-success"><label>' + r.id + '</label></span>';		
			markup+='<label>' + r.localidad + ', ' + r.munabrev + '</label>';
		markup+='</div>';
	markup+='</div>';
	
	return markup;
}

function fmtFirR(r) {
	// console.log("fmtFirmanteR");
	// console.log(r);
	if (r.loading) return r.text;
	if (r.id == "0") return "Teclee un nombre";
	//console.log(r);
	$m ='';
	$m+='<div class="clearfix">';
	$m+='<div class="col-xs-11">' + r.nom;
		if ( r.sex == 1 )
			$m+=' <i class="fa fa-male"></i> - ';
		else 
			$m+=' <i class="fa fa-female"></i> - ';
		
		switch ( r.t ){
			case 1: $m+='<span class="badge alert-info"><i class="fa fa-user-md"></i></span> '+ r.area; break;
			case 2: $m+='<span class="badge alert-info"><i class="fa fa-user-md"></i></span> '+ r.area; break;
			case 3: $m+='<span class="badge alert-danger"><i class="fa fa-university"></i></span> '+ r.mun; break;
			case 4: $m+='<span class="badge alert-danger"><i class="fa fa-university"></i></span> '; break;
			case 5: $m+='<span class="badge alert-warning"><i class="fa fa-h-square"></i></span> ' + r.clues + ' ' + r.area; break;
		}
	$m+='</div>';
	return $m;
}
function fmtFirS(r) {
	// console.log("fmtFirmanteS");
	// console.log(r);
	console.log(r.title);
	if (r.id == "0") return "Escriba un nombre";
	if ( r.id.substring(0, 1)=="A" ){
		alert("de de alta el firmante");
		return;
	}
	var tex="";
	var mon="";
	switch ( r.t ){
			case 1: tex+='text-primary';mon+='<span class="badge alert-info"><i class="fa fa-user-md"></i></span>';break;
			case 2: tex+='text-primary';mon+='<span class="badge alert-info"><i class="fa fa-user-md"></i></span>'; break;
			case 3: tex+='text-danger';mon+='<span class="badge alert-danger"><i class="fa fa-university"></i></span>'; break;
			case 4: tex+='text-danger';mon+='<span class="badge alert-danger"><i class="fa fa-university"></i></span>'; break;
			case 5: tex+='text-warning';mon+='<span class="badge alert-warning"><i class="fa fa-h-square"></i></span>'; break;
	}
	// console.log($tex);
	$("s2_" + r.title ).empty();
	var idTitle = '"'+r.title+'"';
	var tex2= '"'+tex+'"';
	
	$("#s2_" + r.title ).html('<div class="form-group"><div class="row"><div class="col-xs-12"><a onclick="addFirInd()" area_id="' + r.area_id + '" fir_id="' + r.id + '" href="#mdlFirmanteInd" data-toggle="modal" class="btn btn-xs"><i class="fa fa-pencil-square-o"></i></a><a onclick="btnBFir(this)" title='+idTitle+' class="btn btn-xs"><i class="fa fa-eraser"></i></a><label class='+tex2+'>' + r.nom + '</label>' + mon + '</div></div><div class="row"><div class="col-xs-12"><span class='+tex2+'> &nbsp;&nbsp;' + r.cargo + ' &mdash; ' + r.area + '</span></div></div></div>');
	
	return;
}

function fmtCluesMunLocR(r){
	//console.log(r);
	
	if (r.id == "0") return "Escriba una clues...";
	if (r.id == "1"){
		var ouk = r.title.split(",");
		r.id = ouk[0];
		r.abrev = ouk[1];
		r.nombre = ouk[2];
		r.areaAbrev = ouk[3];
	}
	
	
	
	markup = '';
	markup +='<div class="row">';
		markup +='<div class="col-xs-12 ">';
			markup+='<span class="bc_jur">J-'+ r.jur +'</span>';
			markup+='<span class="bc_clu"><i class="fa fa-home" ></i> ' + r.clues + '</span>';		
			markup+='<span class="bc_tpg" title="' + r.tipologia + '">' + r.tpg_id + '</span>';
			markup+='<span class="bc_um">' + r.nombre + '</span>';
		markup+='</div>';			
	markup+='</div>';
	
	markup+='<div class="row">';
		markup +='<div class="col-xs-12">';
			markup+='<span class="bc_mpo"><i class="fa fa-university"></i> ' + r.mun + '</span>';
			markup+='<span class="bc_loc"><i class="fa fa-street-view"></i> ' + r.loc + '</span>';
			markup+='<span class="bc_lcm">' + r.localidad + ', ' + r.municipio + '</span>';
		markup +='</div>';	
	
	markup+='</div>';
	
	return markup; 
}
function fmtCluesMunLocS(r){
	console.log("sel");
	
	if (r.id == "0") return "Escriba una clues...";
	if (r.id == "1"){
		var ouk = r.title.split(",");
		r.id = ouk[0];
		r.abrev = ouk[1];
		r.nombre = ouk[2];
		r.areaAbrev = ouk[3]; 
	}
	
	markup = '';
	markup +='<div class="row" clues_id="'+r.id+'">';
		markup +='<div class="col-xs-12">';
			markup+='<span class="bc_clu"><i class="fa fa-home" ></i> ' + r.clues + '</span>';		
			markup+='<span class="bc_tpg" title="' + r.tipologia + '">' + r.tpg_id + '</span>';
			markup+='<span class="bc_um">' + r.nombre + '</span>';
		markup+='</div>';
	markup+='</div>';
	
	return markup;
}
