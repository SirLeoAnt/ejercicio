// JavaScript Document
/*!
 * AccesoDatos
 * @author: Antonio Ricárdez Hernández
 * @version: 3.0.2 
 * @date: 12 abr 2015
 
 * @coment in @version 1.0.0 @authors: Antonio Ricárdez Hernández, Edgar López, Anuar López Morgan
 
 * @releaseNotes 31/07/2015 : se agregó el spinner <<rim>>, depende de rim.css
 */
myJq={
addJs:function(f){
	var sc = document.getElementsByTagName("script");
	for (var x in sc)if (sc[x].src != null && sc[x].src.indexOf(f) != -1) return;include(f);
},
addCss:function(a){
	$("head").append('<link href="'+a+'" rel="stylesheet" type="text/css" />')
},
addScript:function(a){
	$("head").append('<script type="text/javascript" language="javascript">try{'+a+'}catch( errorObject){console.info( errorObject );}</script>')
},
addStyle:function(a){
	$("head").append("<style>"+a+"</style>")
},
addHtml:function(a,b,c){
	if(c)$(a).html(b);
	else{
		if(a)$("#"+a).html(b);
		else {
			if ($("div#contenedor").length)	$("div#contenedor").empty().slideUp(0).delay(100).html(b).fadeIn(800);
		};
}},
relox: function(r){
	switch(r){
		case 'fb':
			return '<div class="_2iwq"><div class="_2iwr"></div><div class="_2iws"></div><div class="_2iwt"></div><div class="_2iwu"></div><div class="_2iwv"></div><div class="_2iww"></div><div class="_2iwx"></div><div class="_2iwy"></div><div class="_2iwz"></div><div class="_2iw-"></div><div class="_2iw_"></div><div class="_2ix0"></div></div>';
		break;
		case 'pez':
			return '<div width="600px" class="_3iwok"><div class="_y4x10 _w0z0"></div><div class="_y13x3 _w5z12"></div><div class="_y4x6 _w5z11"></div><div class="_y5x8 _w0z18"></div><div class="_y4x10 _w18z16"></div><div class="_y4x9 _w18z0"></div><div class="_y3x1 _w19z9"></div><div class="_y2x1 _w20z10"></div><div class="_y1x2 _w21z11"></div><div class="_y4x3 _w12z6"></div><div class="_y4x3 _w14z0"></div><div class="_y3x1 _w15z3"></div><div class="_y2x1 _w16z4"></div><div class="_y1x2 _w17z5"></div><div class="_y4x3 _w4z0"></div><div class="_y2x2 _w6z1"></div><div class="_y1x1 _w21z15"></div><div class="_y1x4 _w19z11"></div><div class="_y1x2 _w20z13"></div><div class="_y3x1 _w8z0"></div><div class="_y1x1 _w8z1"></div><div class="_y2x1 _w12z0"></div><div class="_y1x1 _w13z1"></div><div class="_y3x2 _w9z2"></div><div class="_y1x4 _w11z1"></div><div class="_y2x2 _w13z4"></div><div class="_y1x2 _w16z7"></div><div class="_y1x1 _w17z9"></div><div class="_y2x2 _w13z4"></div><div class="_y4x1 _w12z5"></div><div class="_y1x1 _w13z3"></div><div class="_y1x1 _w6z3"></div><div class="_y1x2 _w4z5"></div><div class="_y1x3 _w9z4"></div><div class="_y1x1 _w8z3"></div><div class="_y1x2 _w6z5"></div><div class="_y4x3 _w8z8"></div><div class="_y1x3 _w10z5"></div><div class="_y1x1 _w11z7"></div><div class="_y1x1 _w7z7"></div><div class="_y2x2 _w0z10"></div><div class="_y1x1 _w2z10"></div><div class="_y1x2 _w0z12"></div><div class="_y1x4 _w5z7"></div><div class="_y1x1 _w6z8"></div><div class="_y1x1 _w6z10"></div><div class="_y1x1 _w7z9"></div><div class="_y1x4 _w4z10"></div><div class="_y3x2 _w1z14"></div><div class="_y1x2 _w2z12"></div><div class="_y1x1 _w0z17"></div><div class="_y1x1 _w1z16"></div><div class="_y1x1 _w2z17"></div><div class="_y1x1 _w3z16"></div><div class="_y3x1 _w12z9"></div><div class="_y1x1 _w12z10"></div><div class="_y2x1 _w13z11"></div><div class="_y1x8 _w17z18"></div><div class="_y1x4 _w16z20"></div><div class="_y1x2 _w15z21"></div><div class="_y11x1 _w6z17"></div><div class="_y4x1 _w13z16"></div><div class="_y1x1 _w9z16"></div><div class="_y1x1 _w10z15"></div><div class="_y1x1 _w11z16"></div><div class="_y1x1 _w12z15"></div><div class="_y1x1 _w14z15"></div><div class="_y2x1 _w7z18"></div><div class="_y3x1 _w7z19"></div><div class="_y7x1 _w8z20"></div><div class="_y1x1 _w10z18"></div><div class="_y1x1 _w11z19"></div><div class="_y1x1 _w12z18"></div><div class="_y2x2 _w14z18"></div><div class="_y1x1 _w13z19"></div><div class="_y1x3 _w6z20"></div><div class="_y1x1 _w7z21"></div><div class="_y6x2 _w8z22"></div><div class="_y1x1 _w7z23"></div><div class="_y1x1 _w6z24"></div><div class="_y1x1 _w5z25"></div><div class="_y1x1 _w7z25"></div><div class="_y1x1 _w8z24"></div><div class="_y1x1 _w9z25"></div><div class="_y1x1 _w10z24"></div><div class="_y1x1 _w11z25"></div><div class="_y1x1 _w12z24"></div><div class="_y1x1 _w13z25"></div><div class="_y1x2 _w14z23"></div><div class="_y1x1 _w15z24"></div><div class="_y1x1 _w16z25"></div><div class="_y1x6 _w5z18"></div></div>';
		break;
		case 'rim':
			return '<div class="loader"><span class="item-1"></span><span class="item-2"></span><span class="item-3"></span><span class="item-4"></span><span class="item-5"></span><span class="item-6"></span><span class="item-7"></span></div>'
		case 'pb':
			$( "#progressbar" ).progressbar({ value: false });
		break;
		case 'spin': return '<i class="fa fa-spinner fa-spin"></i>'; break;
		case 'circle': return '<i class="fa fa-circle-o-notch fa-spin"></i>'; break;
		case 'refresh': return '<i class="fa fa-refresh fa-spin"></i>'; break;
		case 'cog': return '<i class="fa fa-cog fa-spin"></i>'; break;
		case 'pulse': return '<i class="fa fa-spinner fa-pulse"></i>'; break;
		case 'flip': return '<div class="cp-spinner cp-flip"></div>'; break;
		case 'hue': return '<div class="cp-spinner cp-hue"></div>'; break;
		case 'empty': return '&nbsp;'; break;
	}
}
};
function comunica(u,p,c){
	var c = c || [];
	$.ajax({
		url: u,
		type: 'POST',
		dataType: "json",
		beforeSend: function(){
			//console.log(p);
			$.each(c, function( i, v ) {	
				$( "#" + v.c ).html( myJq.relox(v.t) );
			});
		},
		data:{ parametros:p }
	}).done( function(a){
		if ( typeof a.comunica !== "undefined" ) {
			if( typeof a.comunica.style !== "undefined"){ myJq.addStyle(a.comunica.style); }
			if( typeof a.comunica.css !== "undefined"){myJq.addCss(a.comunica.css);}
			if( typeof a.comunica.html !== "undefined"){myJq.addHtml(a.comunica.contenedor,a.comunica.html);}
			if( typeof a.comunica.hc !== "undefined"){
				$.each(a.comunica.hc, function( i, v ) {	
					$( "#" + v.c ).html( v.h );
				});
			}
			if( typeof a.comunica.hca !== "undefined"){
				$.each(a.comunica.hca, function( i, v ) {	
					$( "#" + v.c ).append( v.h );
				});
			}
			if( typeof a.comunica.js !== "undefined"){myJq.addJs(a.comunica.js);}
			if( typeof a.comunica.script !== "undefined"){myJq.addScript(a.comunica.script);}
			if( typeof a.comunica.template !== "undefined"){myJq.addTemplate(a.comunica.template);}
		} else {
			$("#mdlAlert").modal('show');
			switch ( a.errno ){
					case 1064:
						$("#bdyMdlAlert").empty().append( $("<span>").text(" hay un error de sintaxis en una consulta ") );
					break;
					case 1452:
						$("#bdyMdlAlert").empty().append( $("<span>").text(" no hemos podido agregar o actualizar :¬( -> fk_child ") );
					break;
					default :
						$("#bdyMdlAlert").empty().append( $("<span>").text(" hay algo mal comuniquese a soporte tecnico " + a.errno) );
					break;
				}
			
			console.log(a);
		}
	}).fail(function( e ) {
		console.log(e);
		$("#mdlAlert").modal('show');
		switch (e.status){
			case 404://<i class="fa fa-file-code-o"></i>
				$("#bdyMdlAlert").empty().append(
					$("<center style='background:url(../img/404.png) no-repeat; height: 300px;' >").append(
						$("<i>").addClass("fa fa-10x fa-chain-broken text-danger fa-rotate-135")
					)
				);
			break;
			case 0:
				$("#bdyMdlAlert").empty().append(
					$("<center>").append(
						$("<span>").addClass("fa-stack fa-lg").append(
							$("<i>").addClass("fa fa-wifi fa-stack-5x")
						).append(
							$("<i>").addClass("fa fa-ban fa-stack-10x text-danger")
						)
					)
				);
			break;
			case 200:
				// aqui no puede entrar
				$("#bdyMdlAlert").html("hubo un problema al cargar el modulo");
			break;
			case 505:
				$("#bdyMdlAlert").html("error de 505 ");
			break;
		}
		
		//console.log(error);
		//var $xmlError = $( $.parseXML( error.responseText ) );
		//console.log($xmlError);
		//if($xmlError.find('mensaje').length){
		/*		
		alert($xmlError.find('mensaje').text());
		}
		else alert('algo inesperado ocurrio.. :C');
		if($xmlError.find('mySqlError').length){
			$e = $xmlError.find('mySqlError');
			console.log({
				ERRNO:$e.find('errno').text(),
				ERROR:$e.find('error').text(),
				CONSULTA:$e.find('consulta').text()
			});
		}
		*/
	});
};
if(!Array.prototype.push){Array.prototype.push=function(x){this[this.length]=x;
return true}};
if (!Array.prototype.pop){Array.prototype.pop=function(){var response = this[this.length-1];
this.length--;
return response}};
function include(f){var j = document.createElement("script");j.type = "text/javascript";j.src = f;document.body.appendChild(j);}
function ajaxValidationCallback(status, form, json, options){
			if (window.console) 
			if (status === true) comunicaCallback(json.comunica);
			else if(json.comunica.script) comunicaCallback({ script:json.comunica.script });
}