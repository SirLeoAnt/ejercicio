<?php 
require_once dirname(__FILE__)."/AccesoDatos.php";
class WebResponse extends AccesoDatos{
	private $css='';
	private $style='';	
	private $js='';
	private $html='';	
	private $contenedor='';
	private $hc=array();
	private $hca=array();
	private $script='';
	private $llave='';
	
	public function __construct(){
		$parametros = ( func_num_args() > 0 ) ? ( is_object( func_get_arg(0) ) ) ? func_get_arg(0) : ( is_array( func_get_arg(0) ) ) ? (object)func_get_arg(0) : null : null;
		parent::__construct( $parametros);
	}
	public function append($h,$c){
		$hca = new StdClass();
		$hca->h = utf8_encode($h);
		$hca->c = $c;
		array_push($this->hca,$hca);
	}
	public function mixHtmlContenedor($h,$c){
		$hc = new StdClass();
		$hc->h = utf8_encode($h);
		$hc->c = $c;
		array_push($this->hc,$hc);
	}
	public function getNodo($ndo){
		$encontrado = false;
		foreach( $this->hc as $hc ){
			if( $hc->c == $ndo) $encontrado = true;
		}
		return $encontrado;
	}
	public function preHtmlContenedor($h,$c){
		$hc = new StdClass();
		$hc->h = utf8_encode($h);
		$hc->c = $c;
		array_unshift($this->hc,$hc);
	}
	public function setContenedor($val) { $this->contenedor=$val; }
	public function setScript($val) { $this->script=$val; }
	public function setCss($val) { $this->css=$val; }
	public function setStyle($val) { $this->style=$val; }
	public function setJs($val){ $this->js = $val; }
	public function setHtml($val){ $this->html = $val; }
	public function setTitulo($titulo){ 
		$this->mixHtmlContenedor($titulo,'dda-h1');
	}
	
	public function getValCmb(){ return $this->valCombo;	}
	public function getContenedor() { return $this->contenedor; }
	public function getScript() { return $this->script; }
	public function getCss() { return $this->css; }
	public function getStyle() { return $this->style; }	
	public function getJs() { return $this->js; }
	public function getHtml() { return $this->html; }
	public function getLlave() { return $this->llave; }
	
	public function mixHtml($val) { $this->html.=$val; }
	public function mixStyle($val) { $this->style.=$val; }
	public function mixScript($val) { $this->script.= $val;}
	public function preScript($val) { $this->script = $val.$this->script;}
	
	public function returnJson(){ 
		$arreglo = array();
		//print_r($this);
		if ($this->contenedor!='') $arreglo['contenedor'] = $this->contenedor;
		if ($this->html!=''){ $arreglo['html'] = utf8_encode($this->getHtml());} 	
		if ($this->script!='') $arreglo['script'] = $this->script;
		if ($this->js!='') $arreglo['js'] = $this->js;
		if ($this->css!='') $arreglo['css'] = $this->css;
		if ($this->style!='') $arreglo['style'] = $this->style;
		if (count($this->hc)!=0) $arreglo['hc'] = $this->hc;
		if (count($this->hca)!=0) $arreglo['hca'] = $this->hca;
		$json=new stdClass();
		$json->comunica=$arreglo;
		if (isset( $_SESSION['sso_id'] )){		
			
			/*
			$date1 = new DateTime ($_SESSION['sso_tiempo']);
			$date2 = new DateTime ();
						
			$intervalo = $date1->diff($date2);
			*/
			// echo $intervalo->format("%H:%I:%S") ;
			// intervalos 
			/*
			if($intervalo->format("%H:%I:%S") > '10:00:00'){
				$this->conectar();
				$this->setSql('
					UPDATE tmp_Logueos
					SET logg_fin = "'.(date('Y-m-d h:i:s')).'"
					WHERE logg_id ="'.$_SESSION['sso_logg'].'"
				');
				$this->consultar();	
				
				$_SESSION['sso_tiempo'] = 	date('Y-m-d h:i:s');
			}
			*/
			
			echo json_encode($json);
		}
		else	//
			echo '{"comunica":{"script":"alert(\"tiempo \"); location.href=\"salir.php\""}}';
		
		die();
		
	}
	public function mxDate($d){
		$fecha = strtotime( $d );
		$meses = array('--','Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sept','Oct','Nov','Dic');
		
		if ( date('Y',$fecha) != date('Y') ){
			if ( (int)date('Y',$fecha) != 1969 )
				$f = (int)date('d',$fecha).' de '. $meses[date('n',$fecha)].' del '.date('Y',$fecha);
			else	
				$f = (int)date('d',$fecha).' de '. $meses[date('n',$fecha)].' del '.date('Y',$fecha);
		} else {
			if ( date('md',$fecha) == date('md') ){ //Hoy
				
				$f = 'HOY ';
				//.date('h:m',$fecha);
			}
			else {
				if ( date('m',$fecha) == date('m') && (int)date('d',$fecha) == (int)date('d')-1) 
					$f = 'AYER';
				else if ( date('m',$fecha) == date('m') && (int)date('d',$fecha) == (int)date('d')-2) 
					$f = 'ANTEAYER';
				else
					$f = (int)date('d',$fecha).' de '. $meses[date('n',$fecha)];
			}
		}
		return $f;
	}
	public function fxDate($d){
		$fecha = strtotime( $d );
		$meses = array('--','Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sept','Oct','Nov','Dic');
		$f = (int)date('d',$fecha).' de '. $meses[date('n',$fecha)].' del '.date('Y',$fecha);
		return $f;
	}
	public function rsDate($d){
		$fecha = strtotime( $d );
		$meses = array('--','ENE','FEB','MAR','ABR','MAY','JUN','JUL','AGO','SEP','OCT','NOV','DIC');
		$f = date('Y',$fecha).$meses[date('n',$fecha)].date('d',$fecha);
		return $f;	
	}
	public function nomProp($c){
		$healthy = array(" Del ", " De ", " A ", " En ", " Los ", " Y ", " La ", " Las ");
		$yummy   = array(" del ", " de ", " a ", " en ", " los ", " y ", " la ", " las ");
		return str_replace($healthy, $yummy, ucwords(mb_strtolower( $c )) );
	}
	public function ponCombo(){
		// opciones al construir el combo
		$opc = ( func_num_args() > 0 ) ? ( is_object( func_get_arg(0) ) ) ? func_get_arg(0) : ( is_array( func_get_arg(0) ) ) ? (object)func_get_arg(0) : null : null;
		// atributos reservados 
		$rsrv = array("sql","conexion","values","selecciona","forzarselect","option0","id","name");
		$attr = '';
		// print_r($opc);
		foreach($opc as $k => $v ) {
			if ( ! in_array($k,$rsrv)) 	{
				$attr.= ' '.$k.'="'.$v.'"';
			}
		}
		// combo 
		$cmb = new stdClass();
		$cmb->values = array();
		$attrs = '';
		// dos formas con arreglo o con consulta 
		if (isset($opc->sql)) {
			if (isset($opc->conexion)) 
				$this->conectar($opc->conexion);
			else 
				$this->conectar();
			$this->setSql($opc->sql);
			$this->consultar();
			$nr = $this->getNumRows();
			$nc = $this->getNumeroCampos();
			while ( $r = $this->sacar() ){
				$cmb->values[ $r[0] ] = $r[1];
				for ($i=2; $i < $nc; $i++) {
					$attrs.= $this->getNombreCampo($i).'="'.$r[$i].'" ';
				}
			}
			$this->desconectar();
		} else { // con arreglo
			$values = (isset($opc->values)) ? $opc->values : '';			
			$nr = count($values);
			foreach($values as $k => $v ){
				$cmb->values[ $k ] = $v;
			}
		}
		// combo o input 
		$h = '';
		if($nr>1 or isset($opc->forzarselect)){ // combo
			$h.='<select id="'.$opc->id.'" name="'.$opc->id.'" '.$attr.'>';
			$sx = (isset($opc->selecciona)) ? $opc->selecciona : 0;
			$h.=(isset($opc->option0))?'<option value ="0">'.$opc->option0.'</option>':'<option value ="0">Seleccione una opcion de la lista</option>';
			foreach($cmb->values as $k=>$v){
				$sel = ($sx === $k) ? ' selected="selected"':'';
				$h.='<option value="'.$k.'"'.$sel.' '.$attrs.'>'.$k.' - '.$v.'</option>';
			}
			$h.='</select>';
		} // input
		else {
			foreach($cmb->values as $k=>$v){
				$h.='<input id="'.$opc->id.'" name="'.$opc->id.'"  type="hidden" value="'.$k.'" />';
				$h.='<input id="inp_'.$opc->id.'" name="inp_'.$opc->id.'" '.$attr.' type="text" readonly value="'.$k.' - '.$v.'" />';
				$this->llave = $k;
			}
		}
		return $h;
	}
	public function hexaTint( $hexa, $factor){
		$currentR = hexdec( substr($hexa,0,2) );
		$currentG = hexdec( substr($hexa,2,2) );
		$currentB = hexdec( substr($hexa,4,2) );
		$newR = $currentR + (255 - $currentR) * $factor;
		$newG = $currentG + (255 - $currentG) * $factor;
		$newB = $currentB + (255 - $currentB) * $factor;
		return str_pad(dechex($newR),2,'0',STR_PAD_LEFT).str_pad(dechex($newG),2,'0',STR_PAD_LEFT).str_pad(dechex($newB),2,'0',STR_PAD_LEFT);
		
	}
	public function mxMoneda($n){
		if ($n == '') return '$  0.00';
		return '$  '.number_format($n, 2, '.', ',');
	}
	public function mxNumero($n){
		if ($n == '') return '0';
		return number_format($n, 0, '.', ',');
	}
	public function mxDecimal($n){
		if ($n == '') return '0.00';
		return number_format($n, 2, '.', ',');
	}
	public function bSerie( $serie ){
		$this->conectar("subin");		
		$this->setSql('SELECT serie_id  FROM adc_Serie WHERE serie_pri='.$serie.' and ofi_id='.$_SESSION['sso_ofi'].';');
		$this->consultar();
		if ( $r = $this->extraer() ){ //si existe 
			$serie = $r['serie_id'];
		} else { // si no existe
			$this->setSql('INSERT INTO adc_Serie (serie_pri,ofi_id) VALUES ('.$serie.','.$_SESSION['sso_ofi'].')');
			$this->consultar();
			$serie = $this->getUltimoId();
		}
		return $serie;
	}
	public function bDoc_id( $serie, $anio = '' ){
		$this->conectar("subin");		
		$anio = ( $anio != '' )?$anio:date('Y');
		$this->setSql('select ifnull(MAX(doc_id),0) + 1 as new_id FROM adc_Documento WHERE serie_id='.$serie.' AND doc_anio='.$anio );
		$this->consultar();
		if ( $r = $this->extraer() ){ //si existe 
			$doc_id = $r['new_id'];
		} else { // si no existe
			$doc_id = 0;
		}
		return $doc_id;
	}
	}
	 
?>