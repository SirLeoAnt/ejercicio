<?php 
date_default_timezone_set("America/Mexico_City");
class Conexion {
	private  $servidor;
	private  $usuario;
	private  $password;
	private  $basedatos;
	private  $type;
	
	public function getServidor(){ return $this->servidor; }
	public function getUsuario(){ return $this->usuario; }
	public function getPassword(){ return $this->password; }
	public function getBaseDatos(){ return $this->basedatos; }
	public function getType(){ return $this->type; }

	public function setServidor($val) { $this->servidor= $val; }	
	public function setUsuario($val) { $this->usuario= $val; }	
	public function setPassword($val) { $this->password= $val; }	
	public function setBaseDatos($val) { $this->basedatos= $val; }	
	public function setType($val) { $this->type= $val; }	
	
}
class AccesoDatos{
	private  $acc_conexion_id = 0;
	private  $acc_consulta_id = 0;
	private  $acc_sql = '';
	private  $acc_errno = 0;
	private  $acc_error = "";
	private  $msjerrno = [];
	
	private $serverDefault = 'map17';
	private $acc_server = array();
	private $ultimoId;
	
	/*FUNCIONES SET*/
	public function setConexionId($val) { $this->acc_conexion_id= $val; }		
	public function setConsultaId($val) { $this->acc_consulta_id= $val; }		
	public function setSql($val) { $this->acc_sql= $val; }		
	public function setErrno($val) { $this->acc_errno= $val; }		
	public function setError($val) { $this->acc_error= $val; }	
	public function setUltimoId($val) { $this->ultimoId= $val; }		
	public function addServer($n,$c){ $this->acc_server[$n]=$c; }
	public function setServerDefault($val) { $this->serverDefault= $val; }		
	public function setMsjErrno($k,$val) { $this->msjerrno[$k]= $val; }
	
	/*FUNCIONES GET*/
	public function getUltimoId(){ return $this->ultimoId; }
	public function getID(){ return $this->ultimoId; }
	public function getServer($n){ return $this->acc_server[$n]; }
	public function getServers(){ 
		$a = array();
		foreach ($this->acc_server as $k=>$v){
			$a[$k]=$v->getBaseDatos();
		}
		return $a;
	}
	public function getConexionId(){ return $this->acc_conexion_id; }
	public function getConsultaId(){ return $this->acc_consulta_id; }
	public function getSql(){ return $this->acc_sql; }
	public function getErrno(){ return $this->acc_errno; }
	public function getError(){ return $this->acc_error; }
	public function getMsjErrno(){ return $this->msjerrno; }
	public function mixWhere($w,$m){
		if ($w!='')
			$w = $w.' AND '.$m;
		else 
			$w = ' WHERE '.$m;
		return $w;
	}
	
	
	public function __construct(){
		$prm = ( func_num_args() > 0 ) ? ( is_object( func_get_arg(0) ) ) ? func_get_arg(0) : ( is_array( func_get_arg(0) ) ) ? (object)func_get_arg(0) : null : null;
		
			$c = new Conexion();
			$c->setServidor('localhost');
			$c->setUsuario('root');
			$c->setPassword('');
			$c->setBaseDatos('dbEjercicio');
			$c->setType('MySql');
			$this->addServer('map17',$c);	
			
			$c = new Conexion();
			$c->setServidor('localhost');
			$c->setUsuario('root');
			$c->setPassword('');
			$c->setBaseDatos('information_schema');
			$c->setType('MySql');
			$this->addServer('sch',$c);	
			
			
			
			if	( isset($prm->server) )
				$this->serverDefault = $prm->server;
			
	}
	private function getConexionActiva($n){ 
		$this->serverDefault = ($n=="")?$this->serverDefault:$n;
		if ($this->serverDefault=='') echo "El servidor se ha perdido ".$n;
		return $this->getServer( $this->serverDefault ); 
	}
	public function conectar($n=""){
		$c = $this->getConexionActiva($n);	
		if ( !($c instanceof Conexion) ) {
			echo 'whooops no conecto '.$c;
			// echo $c." no es instancia de CONEXION AL CONECTAR 00";
			//print_r($this);
		}		
		switch($c->getType()){
			case 'MySql':
				$this->setConexionId( mysql_connect( $c->getServidor(), $c->getUsuario(), $c->getPassword(), true) );
				if(!$this->getConexionId()){ 
					$this->setError("Ha fallado la conexion");
					return 0;
				}
				if(!@mysql_select_db($c->getBaseDatos(), $this->getConexionId() )){
					$this->setError( "Imposible accesar ".$c->getBaseDatos() );
					return 0;
				}
			break;
			case 'SqlServer':
				$this->setConexionId( mssql_connect( $c->getServidor(), $c->getUsuario(), $c->getPassword(), true) );
				if(!$this->getConexionId()){ 
					$this->setError("Ha fallado la conexion");
					return 0;
				}
				if(!@mssql_select_db($c->getBaseDatos(), $this->getConexionId() )){
					$this->setError( "Imposible accesar ".$c->getBaseDatos() );
					return 0;
				}
			break;
		}
		//$this->set_char('utf8');
		if ($n!='') $this->serverDefault = $n;
		return $this->getConexionId();
	}	
	public function desconectar(){
		$c = $this->getConexionActiva( $this->serverDefault );
		if( $this->getConexionId() != null){
			switch($c->getType()){
				case 'MySql': mysql_close( $this->getConexionId() ); break; 
				case 'SqlServer': mssql_close( $this->getConexionId() ); break; 
			}
		}
	}
	public function set_char($char){ 
		
		$c = $this->getConexionActiva( $this->serverDefault );
		if( $this->getConexionId() != null){
			switch($c->getType()){
				case 'MySql': mysql_query ( "SET NAMES '$char'",$this->getConexionId() ); break;
				case 'SqlServer': //mssql_query ( "SET NAMES '$char'",$this->getConexionId() ); 
				break;
			}
		}
	}	
	public function consultar(){
		if (func_num_args()==1){
			$this->setSql(func_get_arg(0));
		}			
		if($this->getSql()==''){
			$this->setError("No ha especificado consulta sql");
			$this->setErrno(0);
		}	
		
		$c = $this->getConexionActiva( $this->serverDefault );
		if ( !($c instanceof Conexion) ) {
			echo " no es ".$c." ";
			//print_r($this);
		}
		switch( $c->getType() ){
			case 'MySql': 
				$this->setConsultaId( mysql_query( $this->getSql(),$this->getConexionId() ));
				if( !$this->getConsultaId() ){
					$this->setErrno(mysql_errno());
					$this->setError(mysql_error()); 
					// print_r($this->msjerrno);
					// die();
					$fai = new stdClass();
					$fai->errno = mysql_errno();
					$fai->error = mysql_error();
					$fai->sql = $this->getSql();
					echo json_encode($fai);
					//echo creaXmlError(mysql_errno(),mysql_error(),$this->getSql(),$this->msjerrno);
					die;
				} else {
					if( strpos(strtoupper($this->acc_sql), 'INSERT')!==false ){
						$this->setUltimoId(mysql_insert_id( $this->getConexionId() ));	
						
					}
				}
			break;
			case 'SqlServer': 
				//echo $c->getType()."\n";
				$this->setConsultaId(mssql_query($this->getSql(),$this->getConexionId()));
				if( !$this->getConsultaId() ){
					$this->setErrno(mssql_errno());
					$this->setError(mssql_error());
					echo $this->getSql();				
				}
			break;
		}
		return $this->getConsultaId();
	}
	public function getSqlSlash() { 
		$sql = $this->acc_sql;
		$sql = str_replace("'",'"',$sql);
		return  $sql;
	}	
	public function extraer(){ 
		$c = $this->getServer( $this->serverDefault );
		switch($c->getType()){
			case 'MySql': 
				return mysql_fetch_assoc( $this->getConsultaId() ); 
			break;
			case 'SqlServer': 
				return mssql_fetch_assoc( $this->getConsultaId() ); 
			break;			
		}
	}
	public function sacar(){ 
		$c = $this->getServer( $this->serverDefault );
		switch($c->getType()){
			case 'MySql': 
				return mysql_fetch_array( $this->getConsultaId() ); 
			break;
			case 'SqlServer': 
				return mssql_fetch_array( $this->getConsultaId() ); 
			break;			
		}
	}	
	public function getNumeroCampos(){ 
		$c = $this->getServer( $this->serverDefault );
		if( $this->getConexionId() != null){
			switch($c->getType()){
				case 'MySql': 
					return mysql_num_fields( $this->getConsultaId() ); 
				break; 
				case 'SqlServer': 
					return mssql_num_fields( $this->getConsultaId() ); 	
				break; 
			}
		}
	}
	public function getNumRows(){
		return $this->getNumeroRegistros();
	}
	public function getNumeroRegistros(){ 
		$c = $this->getServer( $this->serverDefault );
		if( $this->getConexionId() != null){
			switch($c->getType()){
				case 'MySql': 
					return mysql_num_rows( $this->getConsultaId() ); 	
				break; 
				case 'SqlServer': 
					return mssql_num_rows( $this->getConsultaId() ); 	
				break; 
			}
		}
		return "algo fallo";
	}
	public function getNombreCampo($n){ 
		$c = $this->getServer( $this->serverDefault );
		if( $this->getConexionId() != null){
			switch($c->getType()){
				case 'MySql': 
					return mysql_field_name($this->getConsultaId(),$n);
				break; 
				case 'SqlServer': 
					return mssql_num_fields( $this->getConsultaId() ); 	
				break; 
			}
		}
	}
	public function getTipoCampo($n){ 
		$c = $this->getServer( $this->serverDefault );
		if( $this->getConexionId() != null){
			switch($c->getType()){
				case 'MySql': 
					return mysql_field_type($this->getConsultaId(),$n);
				break; 
				case 'SqlServer': 
					return mssql_field_type($this->getConsultaId(),$n);
				break; 
			}
		}
	}	
	public function getNumeroModificados(){ 
		$c = $this->getServer( $this->serverDefault );
		if( $this->getConexionId() != null){
			switch($c->getType()){
				case 'MySql': 
					return mysql_affected_rows(); 
				break; 
				case 'SqlServer': 
					return mssql_affected_rows(); 
				break; 
			}
		}		
	}
	public function mysqldate_to_date($fecha){ 			
		date_default_timezone_set('America/Mexico_City');
		
		if($fecha == '' or $fecha == '0000-00-00')
			return "";
		else 
			return date("d/m/Y", strtotime($fecha) ); 	
	}
	public function date_to_mysqldate($fecha){
		if( !strcmp($fecha, "now()") )
			return $fecha;
		else if($fecha != ""){
			$trozos = explode("/",$fecha,3);
			if( strlen($trozos[0])== 2 ){
				return $trozos[2]."/".$trozos[1]."/".$trozos[0];
			}
			else{ /*si ya viene en formato a�o, mes, dia*/
				return $trozos[0]."/".$trozos[1]."/".$trozos[2];
			}
		}
		return "NULL"; 
	}
	public static function getIp(){
		if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		   $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} 
		elseif (isset($_SERVER['HTTP_VIA'])) {
		   $ip = $_SERVER['HTTP_VIA'];
		} 
		elseif (isset($_SERVER['REMOTE_ADDR'])) {
		   $ip = $_SERVER['REMOTE_ADDR'];
		}
		else { 
		   $ip = "unknown";
		}
		
		return $ip;
	}
	}
?>